import 'package:flutter/material.dart';
import 'package:pda_toppfan/Libraries/API/DB/DBLibrary.dart';
import 'package:pda_toppfan/Libraries/Widgets/WidgetLibrary.dart';
import 'package:pda_toppfan/Models/TicketType.dart';

class TicketTypesView extends StatefulWidget {
  TicketTypesView({Key? key}) : super(key: key);

  @override
  TicketTypesViewState createState() => TicketTypesViewState();
}

class TicketTypesViewState extends State<TicketTypesView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(60),
        child: AppbarPDA(
            popType: 'pop', appBarPDATitle: 'Ticket Types Registrados'),
      ),
      body: FutureBuilder<List<TicketType>>(
        future: TicketTypesDB.db.getAllTicketTypes(),
        builder: (BuildContext context, AsyncSnapshot<List<TicketType>> tt) {
          if (tt.hasData) {
            return ListView.builder(
              itemCount: tt.data!.length,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: Text(tt.data![index].name),
                  leading: Text(tt.data![index].id.toString()),
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
