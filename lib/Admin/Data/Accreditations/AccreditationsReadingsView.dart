import 'package:flutter/material.dart';
import 'package:pda_toppfan/Libraries/API/DB/DBLibrary.dart';
import 'package:pda_toppfan/Libraries/GlobalData/GlobalLibrary.dart';
import 'package:pda_toppfan/Models/AccreditationReading.dart';

class AccreditationsReadingsView extends StatefulWidget {
  AccreditationsReadingsView({Key? key}) : super(key: key);

  @override
  _AccreditationsReadingsViewState createState() =>
      _AccreditationsReadingsViewState();
}

class _AccreditationsReadingsViewState
    extends State<AccreditationsReadingsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Acreditaciones',
        ),
        backgroundColor: primaryColor,
        leading: IconButton(
          icon: new Icon(Icons.arrow_back_ios),
          color: lightGray,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
              icon: Icon(Icons.delete),
              onPressed: () async {
                await AccreditationReadingDB.db.deleteAll();
                setState(() {});
              }),
        ],
      ),
      body: FutureBuilder<List<AccreditationReading>>(
        future: AccreditationReadingDB.db.getAllAccreditationReadings(),
        builder: (BuildContext context,
            AsyncSnapshot<List<AccreditationReading>> accreditationReading) {
          if (accreditationReading.hasData) {
            return ListView.builder(
              itemCount: accreditationReading.data!.length,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: Text(accreditationReading.data![index].accreditationId
                      .toString()),
                  leading:
                      Text(accreditationReading.data![index].id.toString()),
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
