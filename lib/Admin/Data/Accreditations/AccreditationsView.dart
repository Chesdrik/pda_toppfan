import 'package:flutter/material.dart';
import 'package:pda_toppfan/Libraries/API/DB/DBLibrary.dart';
import 'package:pda_toppfan/Libraries/GlobalData/GlobalLibrary.dart';
import 'package:pda_toppfan/Libraries/Widgets/WidgetLibrary.dart';
import 'package:pda_toppfan/Models/Accreditation.dart';

class AccreditationsView extends StatefulWidget {
  AccreditationsView({Key? key}) : super(key: key);

  @override
  _AccreditationsViewState createState() => _AccreditationsViewState();
}

class _AccreditationsViewState extends State<AccreditationsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(60),
        child: AppbarPDA(
            popType: 'pop', appBarPDATitle: 'Acreditaciones Registrados'),
      ),
      body: FutureBuilder<List<Accreditation>>(
        future: AccreditationDB.db.getAllAccreditations(),
        builder: (BuildContext context,
            AsyncSnapshot<List<Accreditation>> accreditations) {
          if (accreditations.hasData) {
            return ListView.builder(
              itemCount: accreditations.data!.length,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: Text(accreditations.data![index].name),
                  leading: Text(
                      accreditations.data![index].accreditationId.toString()),
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
