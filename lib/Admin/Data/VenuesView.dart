import 'package:flutter/material.dart';
import 'package:pda_toppfan/Libraries/API/DB/DBLibrary.dart';
import 'package:pda_toppfan/Libraries/Widgets/WidgetLibrary.dart';
import 'package:pda_toppfan/Models/Checkpoint.dart';
import 'package:pda_toppfan/Models/Venue.dart';

class VenuesView extends StatefulWidget {
  VenuesView({Key? key}) : super(key: key);

  @override
  _VenuesViewState createState() => _VenuesViewState();
}

class _VenuesViewState extends State<VenuesView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(60),
        child: AppbarPDA(popType: 'pop', appBarPDATitle: 'Venues Registrados'),
      ),
      body: FutureBuilder<List<Venue>>(
        future: VenuesDB.db.getAllVenues(),
        builder: (BuildContext context, AsyncSnapshot<List<Venue>> venues) {
          if (venues.hasData) {
            return ListView.builder(
              itemCount: venues.data!.length,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: Text(venues.data![index].name),
                  leading: Text(venues.data![index].id.toString()),
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
