import 'package:flutter/material.dart';
import 'package:pda_toppfan/Libraries/API/DB/DBLibrary.dart';
import 'package:pda_toppfan/Libraries/Widgets/WidgetLibrary.dart';
import 'package:pda_toppfan/Models/Checkpoint.dart';

class CheckpointsView extends StatefulWidget {
  CheckpointsView({Key? key}) : super(key: key);

  @override
  _CheckpointsViewState createState() => _CheckpointsViewState();
}

class _CheckpointsViewState extends State<CheckpointsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(60),
        child: AppbarPDA(
            popType: 'pop', appBarPDATitle: 'Checkpoints Registrados'),
      ),
      body: FutureBuilder<List<Checkpoint>>(
        future: CheckpointsDB.db.getAllCheckpoints(),
        builder: (BuildContext context, AsyncSnapshot<List<Checkpoint>> check) {
          if (check.hasData) {
            return ListView.builder(
              itemCount: check.data!.length,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: Text(check.data![index].name),
                  leading: Text(check.data![index].id.toString()),
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
