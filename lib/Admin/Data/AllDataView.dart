import 'package:flutter/material.dart';
import 'package:pda_toppfan/Libraries/Widgets/WidgetLibrary.dart';

class AllDataView extends StatefulWidget {
  AllDataView({Key? key}) : super(key: key);

  @override
  _AllDataViewState createState() => _AllDataViewState();
}

class _AllDataViewState extends State<AllDataView> {
  @protected
  @mustCallSuper
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(60),
        child: AppbarPDA(popType: 'pop', appBarPDATitle: 'Datos Registrados'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30.0),
        child: Column(
          children: <Widget>[
            SizedBox(height: 20.0),
            ExpansionTile(
              title: Text(
                "Tickets",
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
              children: <Widget>[
                ListTile(
                  title: Text('Registrados'),
                  onTap: () {
                    Navigator.pushNamed(context, '/tickets');
                  },
                ),
                ListTile(
                  title: Text('Lecturas'),
                  onTap: () {
                    Navigator.pushNamed(context, '/tickets_reading');
                  },
                )
              ],
            ),
            ExpansionTile(
              title: Text(
                "Acreditaciones",
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
              children: <Widget>[
                ListTile(
                  title: Text('Registrados'),
                  onTap: () {
                    Navigator.pushNamed(context, '/accreditations/registered');
                  },
                ),
                ListTile(
                  title: Text('Lecturas'),
                  onTap: () {
                    Navigator.pushNamed(context, '/accreditations/readings');
                  },
                )
              ],
            ),
            ExpansionTile(
              title: Text(
                "Datos almacenados",
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
              children: <Widget>[
                ListTile(
                  title: Text('Checkpoints'),
                  onTap: () {
                    Navigator.pushNamed(context, '/checkpoints');
                  },
                ),
                ListTile(
                  title: Text('Venues'),
                  onTap: () {
                    Navigator.pushNamed(context, '/venues');
                  },
                ),
                ListTile(
                  title: Text('Ticket Types'),
                  onTap: () {
                    Navigator.pushNamed(context, '/ticket_types');
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
