import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:pda_toppfan/Libraries/API/DB/DBLibrary.dart';
import 'package:pda_toppfan/Libraries/API/apiLibrary.dart';
import 'package:pda_toppfan/Libraries/GlobalData/GlobalLibrary.dart';
import 'package:pda_toppfan/Libraries/Helpers/HelpersLibrary.dart';
import 'package:pda_toppfan/Models/TicketReading.dart';

class TicketsReadingView extends StatefulWidget {
  TicketsReadingView({Key? key}) : super(key: key);

  @override
  _TicketsReadingViewState createState() => _TicketsReadingViewState();
}

class _TicketsReadingViewState extends State<TicketsReadingView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
            'Tickets leidos',
          ),
          backgroundColor: primaryColor,
          leading: IconButton(
            icon: new Icon(Icons.arrow_back_ios),
            color: lightGray,
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions: [
            IconButton(
                icon: Icon(Icons.sync),
                onPressed: () async {
                  await syncTicketReadings(context);
                  print('synced');
                }),
            IconButton(
                icon: Icon(Icons.delete),
                onPressed: () async {
                  print('delete!');
                  await TicketReadingsDB.db.deleteAll();
                  setState(() {});
                })
          ]),
      body: FutureBuilder<List<TicketReading>>(
        future: TicketReadingsDB.db.getAllTicketReadings(),
        builder:
            (BuildContext context, AsyncSnapshot<List<TicketReading>> tickets) {
          if (tickets.hasData) {
            return ListView.builder(
              itemCount: tickets.data!.length,
              itemBuilder: (BuildContext context, int index) {
                // Ticket _ticket = await tickets.data[index].getTicket();

                return ListTile(
                  // title: Text(tickets.data[index].amount.toString() +
                  //     " x " +
                  //     tickets.data[index].name),
                  // title: Text(_ticket.name),
                  title: Text(tickets.data![index].readingTime),
                  leading: Text(tickets.data![index].id.toString()),
                  onTap: (() {
                    // Navigator.pushNamed(context, '/checkpoint',
                    //     arguments: item.id.toString());
                    print('tap that ass');
                  }),
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Future<void> syncTicketReadings(BuildContext context) async {
    // Showing alert
    showReadingAlert(context);

    // Fetching device id
    String _deviceId = await getDeviceId();

    // Fetching ticket readings from DB
    List<TicketReading> _ticketReadings =
        await TicketReadingsDB.db.getAllTicketReadings();

    print('Uploading');
    for (TicketReading _ticketReading in _ticketReadings) {
      print(_ticketReading.readingTime);

      Map<String, dynamic> jsonData = await fetchAPIDataPOST(
          "/pda/tickets/upload",
          {
            'ticket_id': _ticketReading.ticketId.toString(),
            'checkpoint_id': _ticketReading.checkpointId.toString(),
            'type': (_ticketReading.access ? 'access' : 'exit'),
            'error': (_ticketReading.error ? '1' : '0'),
            'reading_time': _ticketReading.readingTime,
            'device_id': _deviceId,
          },
          'qrserver');
    }

    // Navigator.pop(context);
    Navigator.of(context, rootNavigator: true).pop();
  }

  void showReadingAlert(BuildContext context) {
    Alert(
      context: context,
      type: AlertType.info,
      title: "Subiendo lecturas",
      desc: "Espere un momento por favor ",
    ).show();
  }
}
