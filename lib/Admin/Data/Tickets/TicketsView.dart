import 'package:flutter/material.dart';
import 'package:pda_toppfan/Libraries/API/DB/DBLibrary.dart';
import 'package:pda_toppfan/Libraries/Widgets/WidgetLibrary.dart';
import 'package:pda_toppfan/Models/Ticket.dart';

class TicketsView extends StatefulWidget {
  TicketsView({Key? key}) : super(key: key);

  @override
  _TicketsViewState createState() => _TicketsViewState();
}

class _TicketsViewState extends State<TicketsView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(60),
        child: AppbarPDA(popType: 'pop', appBarPDATitle: 'Tickets Registrados'),
      ),
      body: FutureBuilder<List<Ticket>>(
        future: TicketDB.db.getAllTickets(),
        builder: (BuildContext context, AsyncSnapshot<List<Ticket>> tickets) {
          if (tickets.hasData) {
            return ListView.builder(
              itemCount: tickets.data!.length,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  title: Text(tickets.data![index].name),
                  leading: Text(tickets.data![index].ticketId.toString()),
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
