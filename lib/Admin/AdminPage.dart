import 'package:flutter/material.dart';
import 'package:pda_toppfan/Libraries/API/DB/DBLibrary.dart';
import 'package:pda_toppfan/Libraries/Helpers/HelpersLibrary.dart';
import 'package:pda_toppfan/Libraries/Widgets/WidgetLibrary.dart';
import 'package:pda_toppfan/Models/Configuration.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AdminPage extends StatefulWidget {
  AdminPage({Key? key}) : super(key: key);

  @override
  _AdminPageState createState() => _AdminPageState();
}

class _AdminPageState extends State<AdminPage> {
  @protected
  @mustCallSuper
  late List<Configuration> list;
  late Configuration conf;
  bool existConf = false;
  String deviceId = '';
  bool offline = false;

  void initState() {
    super.initState();
    _conf();
  }

  _conf() async {
    var prefs = await SharedPreferences.getInstance();
    bool auxOffline = prefs.getBool('offline') ?? true;
    print(auxOffline);
    list = await ConfigurationDB.db.getAllConfigurations();
    String aux = await getDeviceId();
    setState(() {
      this.deviceId = aux;
      this.offline = auxOffline;
    });
    if (list.isNotEmpty) {
      Configuration? aux = await ConfigurationDB.db.getConfig();
      setState(() {
        this.existConf = true;
        print(this.existConf);
        this.conf = aux!;
        conf = this.conf;
      });
    } else {
      print(this.existConf);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(60),
        child: AppbarPDA(popType: 'pop', appBarPDATitle: 'Administración'),
      ),
      body: ListView(
        children: <Widget>[
          existConf
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      'Configuración actual:',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text("Sede: " + conf.venueName),
                    Text("Punto de acceso: " + conf.checkpointName),
                    Text('Tipo de lectura: ' + conf.type),
                    SizedBox(
                      height: 30,
                    ),
                  ],
                )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    Text('Aun no existe una configuración',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    SizedBox(
                      height: 30,
                    ),
                  ],
                ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text('Id del dispositivo: ' + this.deviceId),
              SizedBox(
                height: 10,
              ),
              Text('Modo Offline: ' + (offline ? 'activado' : 'desactivado')),
              SizedBox(
                height: 10,
              ),
              Switch(
                value: offline,
                onChanged: (value) async {
                  final prefs = await SharedPreferences.getInstance();
                  setState(() {
                    offline = value;
                    prefs.setBool('offline', offline);
                    print(offline);
                  });
                },
                activeTrackColor: Colors.lightGreenAccent,
                activeColor: Colors.green,
              ),
            ],
          ),
          HomeButton(title: 'Configurar PDA', action: '/configuration/type'),
          HomeButton(title: 'Descargar APK', action: 'download'),
          HomeButton(title: 'Datos almacenados', action: '/all_data'),
        ],
      ),
    );
  }
}
