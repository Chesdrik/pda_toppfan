import 'package:flutter/material.dart';
import 'package:pda_toppfan/Admin/Configuration/ConfigurationViews/ConfigurationView.dart';
import 'package:pda_toppfan/Libraries/API/apiLibrary.dart';
import 'package:pda_toppfan/Libraries/Widgets/WidgetLibrary.dart';

class ConfigurationFetch extends StatefulWidget {
  ConfigurationFetch({Key? key}) : super(key: key);

  @override
  ConfigurationFetchState createState() => new ConfigurationFetchState();
}

class ConfigurationFetchState extends State<ConfigurationFetch> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: syncAllData(),
        builder: (BuildContext context, AsyncSnapshot event) {
          return (event.data == null
              ? Loading()
              : ConfigurationView(
                  events: event.data,
                ));
        });
  }
}
