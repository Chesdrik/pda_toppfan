import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pda_toppfan/Libraries/API/DB/DBLibrary.dart';
import 'package:pda_toppfan/Libraries/API/apiLibrary.dart';
import 'package:pda_toppfan/Libraries/Widgets/WidgetLibrary.dart';
import 'package:pda_toppfan/Models/DDEntry.dart';
import 'package:pda_toppfan/Libraries/GlobalData/GlobalLibrary.dart' as globals;
import 'package:pda_toppfan/Models/ResponseModel.dart';

// ignore: must_be_immutable
class ConfigurationView extends StatefulWidget {
  List<DDEntry> events;
  ConfigurationView({required this.events, Key? key}) : super(key: key);

  @override
  _ConfigurationViewState createState() => _ConfigurationViewState();
}

class _ConfigurationViewState extends State<ConfigurationView> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  var idVenue = '0';
  var idCheck = '0';
  var idEvent = '0';
  var type = '0';
  var venueName = '';
  var checkpointName = '';
  var eventName = '';
  int _index = 0;
  List<DDEntry> venues = [];
  List<DDEntry> checkpoints = [];
  List<DDEntry> types = [];
  bool update = false;

  void initState() {
    super.initState();
    print('Tipo de lectura:' + globals.type);
    _getEntryVenues();
    this.idEvent = this.widget.events.first.key;
  }

  _getEntryVenues() async {
    List<DDEntry> aux = await VenuesDB.db.getEntryVenue();
    setState(() {
      this.venues = aux;
      print(this.venues);
      this.idVenue = this.venues.first.key;
    });
  }

  _getEntryCheckpoint(var value) async {
    List<DDEntry> aux =
        await CheckpointsDB.db.getEntryCheckpoint(int.parse(value));

    setState(() {
      this.checkpoints = aux;
      print('Config checkpoints');
      print(this.checkpoints);
      this.idCheck = this.checkpoints.first.key;
    });
  }

  _updateVenue(_newValue) async {
    setState(() {
      this.idVenue = _newValue;
      this.venueName = this
          .venues
          .where((element) => (element.key.contains(this.idVenue)))
          .first
          .value;
      print(idVenue);
    });
    await _getEntryCheckpoint(_newValue);
  }

  _updateCheckpoint(_newValue) {
    setState(() {
      this.idCheck = _newValue;
      this.checkpointName = this
          .checkpoints
          .where((element) => (element.key.contains(this.idCheck)))
          .first
          .value;
      print(idCheck);
    });
  }

  _updateEvent(_newValue) {
    setState(() {
      this.idEvent = _newValue;
      // rows.where((row) => (row["name"].contains(value)))
      this.eventName = widget.events
          .where((element) => (element.key.contains(this.idEvent)))
          .first
          .value;

      print(idEvent);
    });
  }

  String title = globals.type == 'access' ? 'accesos' : 'tickets';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(60),
        child: AppbarPDA(
            popType: 'pop', appBarPDATitle: 'Congiguración para ' + title),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          children: <Widget>[
            Stepper(
              controlsBuilder: (BuildContext context,
                  {VoidCallback? onStepContinue, VoidCallback? onStepCancel}) {
                return Row(
                  children: <Widget>[
                    TextButton(
                      onPressed: onStepContinue,
                      child: const Text('Siguiente'),
                    ),
                    TextButton(
                      onPressed: onStepCancel,
                      child: const Text('Cancelar'),
                    ),
                  ],
                );
              },
              currentStep: _index,
              onStepCancel: () {
                if (_index <= 0) {
                  return;
                }
                setState(() {
                  _index--;
                });
              },
              onStepContinue: () {
                if (_index > 1) {
                  return;
                }
                setState(() {
                  _index++;
                });
              },
              onStepTapped: (index) {
                setState(() {
                  _index = index;
                });
              },
              steps: [
                Step(
                  title: Text("Sede"),
                  content: Container(
                    alignment: Alignment.centerLeft,
                    child: Column(
                      children: <Widget>[
                        // Text('Por favor, selecciona uno'),
                        Dropdown(
                            'Sede', this.idVenue, this.venues, _updateVenue),
                      ],
                    ),
                  ),
                ),
                globals.type == 'tickets'
                    ? Step(
                        title: Text("Eventos"),
                        content: Column(
                          children: <Widget>[
                            // Text('Por favor, selecciona uno'),
                            Dropdown('Eventos', this.idEvent,
                                this.widget.events, _updateEvent)
                          ],
                        ),
                      )
                    : Step(
                        state: StepState.disabled,
                        title:
                            Text("No se puede configurar eventos para accesos"),
                        content: SizedBox(
                          width: 0,
                          height: 0,
                        ),
                      ),
                Step(
                  title: Text("Checkpoint"),
                  content: Column(
                    children: <Widget>[
                      // Text('Por favor, selecciona uno'),
                      Dropdown('Checkpoint', this.idCheck, this.checkpoints,
                          _updateCheckpoint),
                    ],
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Text(
                  'Configuración:',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(this.venueName),
                Text(this.eventName),
                Text(this.checkpointName),
                // Text(globals.conf.type),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
            ButtonTheme(
              minWidth: 200.0,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(globals.primaryColor),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  )),
                ),
                child: Text('GUARDAR',
                    style: TextStyle(fontSize: 20, color: Colors.white)),
                onPressed: () async {
                  Dialogs.showLoadingDialog(context, _keyLoader);
                  ResponseModel response = await updateConfig(
                      int.parse(this.idVenue),
                      int.parse(this.idCheck),
                      globals.type,
                      idEvent: int.parse(
                          this.idEvent)); // Fetches all tickets by checkpoint
                  Fluttertoast.showToast(
                    msg: response.message,
                    backgroundColor: response.error ? Colors.red : Colors.green,
                  );
                  setState(() {}); //go to qr navigation
                  if (!response.error) {
                    Navigator.pushNamed(context, '/');
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
