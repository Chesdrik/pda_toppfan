import 'package:flutter/material.dart';
import 'package:pda_toppfan/Libraries/GlobalData/GlobalLibrary.dart';
import 'package:pda_toppfan/Libraries/Widgets/WidgetLibrary.dart';
import 'package:pda_toppfan/Libraries/GlobalData/GlobalLibrary.dart' as globals;

class SelectActionView extends StatefulWidget {
  SelectActionView({Key? key}) : super(key: key);

  @override
  _SelectActionViewState createState() => _SelectActionViewState();
}

class _SelectActionViewState extends State<SelectActionView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(60),
        child: AppbarPDA(
            popType: 'pop', appBarPDATitle: 'Seleccionar tipo de lectura'),
      ),
      body: Center(
          child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Spacer(),
            ButtonTheme(
              minWidth: double.infinity,
              height: 80,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(primaryColor),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  )),
                ),
                child: Text(
                  'Leer Tickets',
                  style: TextStyle(
                    color: lightGray,
                    fontSize: 20,
                    letterSpacing: 3,
                  ),
                ),
                onPressed: () => {
                  globals.type = 'tickets',
                  Navigator.pushNamed(context, '/configuration'),
                },
              ),
            ),
            SizedBox(height: 30),
            ButtonTheme(
              minWidth: double.infinity,
              height: 80,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(primaryColor),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  )),
                ),
                child: Text(
                  'Leer Accesos',
                  style: TextStyle(
                    color: lightGray,
                    fontSize: 20,
                    letterSpacing: 3,
                  ),
                ),
                onPressed: () => {
                  globals.type = 'access',
                  Navigator.pushNamed(context, '/configuration'),
                },
              ),
            ),
            Spacer()
          ],
        ),
      )),
    );
  }
}
