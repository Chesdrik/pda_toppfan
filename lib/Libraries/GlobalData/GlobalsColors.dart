part of global_data;
// import 'package:pda_toppfan/Helpers/helper.dart';

// Colors
Color primaryColor = HexColor('#374564');
Color secondaryColor = HexColor('#4CB9E4');

// https://www.materialpalette.com/colors
Color lightGray = HexColor('#f5f5f5'); // 100R
Color mediumGray = HexColor('#eeeeee'); // 200ok,
Color darkGray = HexColor('#9e9e9e'); // 500
Color darkerGray = HexColor('#616161'); // 700
Color red = HexColor('#EF5350');
Color orange = HexColor('#FF5722');

// New colors
Color buttonBackgroundColor = HexColor('#374564');
Color buttonTextColor = Colors.white;
