part of global_data;

//General variables
String type = 'access';
Venue? venue;
Checkpoint? check;
Configuration? conf;
int event = 0;
bool production = true;
bool debug = true;
bool reading = true;

// Session variables
// User user;
bool isLogged = false;
bool scannerPDA = false;
bool readError = false;
bool errorExit = false;
bool multipleExit = false;
