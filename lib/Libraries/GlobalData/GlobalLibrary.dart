library global_data;

// Required imports
import 'package:flutter/material.dart';
import 'package:pda_toppfan/Libraries/Helpers/HelpersLibrary.dart';
import 'package:pda_toppfan/Models/Checkpoint.dart';
import 'package:pda_toppfan/Models/Configuration.dart';
import 'package:pda_toppfan/Models/Venue.dart';

// Parts
part 'package:pda_toppfan/Libraries/GlobalData/GlobalsColors.dart';
part 'package:pda_toppfan/Libraries/GlobalData/GlobalURL.dart';
part 'package:pda_toppfan/Libraries/GlobalData/GlobalSessions.dart';
