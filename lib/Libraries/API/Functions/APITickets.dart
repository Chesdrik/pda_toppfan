part of api_library;

/// Obtain Tickets by checkpoint
/// {@category Config}
/// {@subCategory Information displays}
Future<void> getTicketsCheckpoint() async {
  // Cleaning checkpoint db
  await TicketDB.db.deleteAll();

  // Fetching data
  Map<String, dynamic> jsonData = await fetchAPIDataPOST(
      "/tickets/sync/checkpoint",
      {'checkpoint_id': globals.check!.id.toString()},
      'qrserver');

  if (jsonData['tickets'] != null) {
    for (var e in jsonData['tickets']) {
      List<int> checkpoint = [];
      for (var c in e['checkpoints']) {
        checkpoint.add(c);
      }

      Ticket ticket = new Ticket(
        id: 0,
        ticketId: e['id'],
        eventId: e['eventId'],
        ticketTypeId: e['ticketTypeId'],
        name: e['name'],
        orderId: e['orderId'],
        amount: e['amount'],
        checkpoints: checkpoint,
      );
      print(ticket.toMap());
      await TicketDB.db.newTicket(ticket);
    }
  }
}

/// Sync access from pumas
/// {@category Access}
/// {@subCategory Information displays}
Future<List<Access>> getAccess() async {
  await AccessDB.db.deleteAll();
  Map<String, dynamic> jsonData = await fetchAPIDataPOST(
      "/sync/acccess/list",
      {
        'venue_id': globals.venue!.id.toString(),
      },
      'qrserver');

  List<Access> access = [];

  // ignore: unnecessary_null_comparison
  bool check = jsonData == null;

  print(jsonData);
  if (check) {
    return access;
  } else {
    for (var e in jsonData['accesses']) {
      print(e);
      Access ac = new Access(
        idAccess: e['id'],
        userId: e['userId'],
        venueId: e['venueId'],
        visitorName: e['visitorName'],
        start: e['start'],
        end: e['end'],
        type: e['type'],
      );

      for (var re in e['requirements']) {
        print(re['type']);
        RequirementAccess ra = new RequirementAccess(
            idAccess: e['id'],
            accessRequirementId: re['accessRequirementId'],
            type: re['type'],
            request: re['request'],
            dataType: re['dataType'],
            status: 'false',
            id: 0);

        bool venueexist = await AccessDB.db.getAccessByID(ac.idAccess);

        if (!venueexist) {
          await AccessDB.db.newAccess(ac);
          await AccessRequirementsDB.db.newRequirementAccess(ra);
        } else {
          await AccessDB.db.updateConfiguration(ac);
          await AccessRequirementsDB.db.updateRequirementAccess(ra);
        }
      }
      access.add(ac);
    }
  }
  globals.event = 0;
  return access;
}

/// Sync Tickets Readings with synchronized in false
/// {@category Access}
/// {@subCategory Information displays}
Future<void> ticketsSynchronization() async {
  List<TicketReading> ticketsreadingNotSynchronized =
      await TicketReadingsDB.db.getNotSynchronizedTicketReading();

  // if (ticketsreadingNotSynchronized.isEmpty) {
  //   print('not send data');
  // } else {
  var encoded = jsonEncode(ticketsreadingNotSynchronized.toList());
  String deviceId = await getDeviceId();

  print(jsonEncode(<String, String>{
    'data': encoded,
    'checkpointID': globals.conf!.checkpointID.toString(),
    'deviceID': deviceId
  }));

  Map<String, dynamic> jsonData = await fetchAPIDataPOST(
      "/pda/tickets/synchronization",
      {
        'data': encoded,
        'checkpointID': globals.conf!.checkpointID.toString(),
        'deviceID': deviceId
      },
      'qrserver');

  if (!jsonData['error']) {
    for (var ticket in ticketsreadingNotSynchronized) {
      await TicketReadingsDB.db.deleteTicketReading(ticket.id);
    }
  }

  if (jsonData['tickets'] != null) {
    for (var e in jsonData['tickets']) {
      //search into local db
      bool existTicket = await TicketReadingsDB.db
          .existsTicketReadingTicketReadingId(e['ticket_id']);
      //update ticket_reading_id

      //before to insert, check if exits into device, if it doesn't exist, then insert
      if (!existTicket) {
        TicketReading ticket = new TicketReading(
            id: 0,
            orderId: e['orderId'],
            ticketId: e['ticket_id'], //id from db qrserver
            checkpointId: e['checkpoint_id'],
            error: (e['error'] == 0 ? false : true),
            synchronized: true,
            readingTime: e['reading_time'],
            access: (e['type'] == 'access' ? true : false),
            deviceId: e['device_id'],
            errorMessage: '');
        await TicketReadingsDB.db.insertTicketReading(ticket, insert: false);
      }
    }
  }
}

/// Process scanner ticket qr.
///
/// {@category Scanner Ticket}
/// {@subCategory Information QR}
Future<void> processScannedTicketQR(
    BuildContext context, Map<String, dynamic> qr, bool _readingAccess) async {
  print('Procesando ticket');
  print(qr.toString());

  // Defining date
  var now = new DateTime.now();
  String date = DateFormat('yyyy-MM-dd HH:mm:ss').format(now).toString();

  // Access or exit
  print('Acceso: ' + _readingAccess.toString());

  // Fetching ticket from local DB
  var ticket = await TicketDB.db.getTicketByID(qr['id']);
  print('ticket');
  print(ticket);

  final prefs = await SharedPreferences.getInstance();
  bool offline = prefs.getBool('offline') ?? true;
  print('offiline mode');
  print(offline);

  //if it is not in the device it is because the ticket is in the wrong checkpoint
  if (ticket == null) {
    // Poping alert
    Navigator.pop(context);

    // print("Checkpoints correctos");
    // List<int> _ticketCheckpoints = qr['checkpoints'].cast<int>();
    // print(qr['checkpoints'].toString());
    // print(_ticketCheckpoints.toString());
    // print('end');

    // // Checking correct access
    // String _checkpointString =
    //     await generateCorrectCheckpointAccessStringForTicket(
    //         _ticketCheckpoints);
    // print('Checkpoint string');
    // print(_checkpointString);
    // catchError(qr['id'], 'ticket', 'checkpoint', qr, date);
    // // Showing alert with correct checkpoint
    // showAlert(context, true, "Acceso incorrecto",
    //     "Dirigir a accesos " + _checkpointString);

    //por lo mientras
    showAlert(context, true, "Acceso incorrecto",
        "El acceso por esta puerta está denegado");
  } else if (ticket.checkpoints.contains(globals.conf!.checkpointID)) {
    print('Correct checkpoint');
    // Correct checkpoint
    // Getting device ID
    String deviceId = await getDeviceId();

    // Making request
    String _readingTime =
        DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now()).toString();

    String _readingHash = "123";
    // await generateReadingHash(accreditation, _readingTime);

    if (offline) {
      //modo offline activado (osea sin internet) hace la validación local directamente
      await offlineScannedTicket(context, qr, date, _readingAccess, ticket);

      // Popping alert
      Navigator.pop(context);
    } else {
      // si el modo offline esta desactivado verifica con el servidor
      print('Requesting');
      var response = await fetchAPIDataPOST(
          '/pda/tickets/access',
          {
            'readingTime': _readingTime,
            'ticketId': ticket.ticketId.toString(),
            'checkpointId': globals.conf!.checkpointID.toString(),
            'device_id': deviceId,
            'type': (_readingAccess ? 'access' : 'exit'),
          },
          'qrserver');
      print('Requesting');
      print(response);

      if (response == null) {
        // Lectura en local
        print('Server timeout, local validation');

        await offlineScannedTicket(context, qr, date, _readingAccess, ticket);
      } else {
        // Lectura en el server
        if (response['error']) {
          // Create reading error
          print('Inserting error reading');
          print(response['message']);

          // Inserting reading into db
          await TicketReadingsDB.db.insertTicketReading(TicketReading(
            id: 0,
            orderId: ticket.orderId,
            ticketId: ticket.ticketId,
            checkpointId: globals.conf!.checkpointID,
            error: true,
            synchronized: true,
            readingTime: _readingTime,
            access: _readingAccess,
            deviceId: deviceId,
            errorMessage: response['message'],
          ));
        } else {
          // Create reading success
          print('Inserting success reading');

          // Inserting reading into db
          await TicketReadingsDB.db.insertTicketReading(TicketReading(
            id: 0,
            orderId: ticket.orderId,
            ticketId: ticket.ticketId,
            checkpointId: globals.conf!.checkpointID,
            error: false,
            synchronized: true,
            readingTime: _readingTime,
            access: _readingAccess,
            deviceId: deviceId,
            errorMessage: '',
          ));
        }
      }
      // Popping alert
      Navigator.pop(context);
    }
  } else {
    print('acceso incorrecto');
    catchError(qr['id'], 'ticket', 'checkpoint', qr, date,
        'processScannedTicketQR incorrect access');
    // Poping alert
    Navigator.pop(context);

    // // Checking correct access
    // String _checkpointString =
    //     await generateCorrectCheckpointAccessStringForTicket(ticket);

    // // Showing alert with correct checkpoint
    // showAlert(context, true, "Acceso incorrecto",
    //     "Dirigir a accesos " + _checkpointString);
  }
  globals.reading = true;
  return null;
}

/// Offline validation for tickets readings
/// {@category Scanner Ticket}
/// {@subCategory Information QR}
Future offlineScannedTicket(context, qr, date, _readingAccess, ticket) async {
  //modo offline activado (osea sin internet) hace la validación local directamente
  // Lectura en local
  print('local validation, tickets');

  // Checking if accreditation exists on local DB
  bool ticketExist = await TicketDB.db.ticketExistByticketId(qr['id']);

  print('Checking if ticket exists: ' + ticketExist.toString());

  if (ticketExist) {
    await validateTicketAccessExitLocal(
        TicketReading(
          id: 0,
          orderId: qr['orderId'],
          ticketId: qr['id'],
          checkpointId: globals.conf!.checkpointID,
          error: false, // check
          synchronized: false,
          readingTime: date,
          access: _readingAccess,
          deviceId: await getDeviceId(), errorMessage: '',
        ),
        ticket,
        _readingAccess);
  } else {
    // Does not exist in local database
    catchError(qr['id'], 'ticket', 'event', qr, date,
        'offlineScannedTicket not into device');
    showAlert(context, true, "Error", "QR inválido");

    return false;
  }
  globals.reading = true;
}

/// Validate access exit local into db
/// {@category Scanner Ticket}
/// {@subCategory Information QR}
Future validateTicketAccessExitLocal(
    TicketReading _ticketReading, Ticket _ticket, bool access) async {
  try {
    if (access) {
      await TicketReadingsDB.db.verifyAccess(_ticketReading, _ticket);
    } else {
      await TicketReadingsDB.db.verifyExit(_ticketReading, _ticket);
    }
  } catch (e) {
    print(e);
  }
}
