part of api_library;

/// {@category Basics}
/// {@category URI, Request, POST}
/// {@subCategory Information displays}
Uri uriConstructor(String endpoint, arguments, String server) {
  String _requestURL =
      (server == 'main' ? globals.mainserverURL : globals.qrserverURL);

  print('Fetching from ' + _requestURL + globals.urlSuffix + endpoint);

  if (globals.production) {
    return Uri.https(_requestURL, globals.urlSuffix + endpoint, arguments);
  } else {
    return Uri.http(_requestURL, globals.urlSuffix + endpoint, arguments);
  }
}

/// {@category Basics}
/// {@category URI, Request, FetchData, POST}
/// {@subCategory Information displays}
Future<Map<String, dynamic>> fetchAPIDataPOST(
    String requestURL, Map<String, String> arguments, String server) async {
  print("POST:" + requestURL);
  print("Arguments:" + arguments.toString());

  try {
    Uri uri = uriConstructor(requestURL, arguments, server);
    var response = await http.post(uri); //timeout(const Duration(seconds: 3));

    return await parseResponse(response);
  } on TimeoutException catch (e) {
    print('Timeout');
    return throw Exception(e);
  } catch (e) {
    print(e);
    return throw Exception(e);
  }
}

/// {@category Basics}
/// {@category URI, Request, FetchData, GET}
/// {@subCategory Information displays}
Future<Map<String, dynamic>> fetchAPIDataGET(
    String requestURL, String server) async {
  print("GET:" + requestURL);

  try {
    Uri uri = uriConstructor(requestURL, {}, server);
    var response = await http.get(uri);

    return parseResponse(response);
  } catch (e) {
    print(e);
    return throw Exception(e);
  }
}

/// {@category Basics}
/// {@category URI, Request, parseResponse}
/// {@subCategory Information displays}
Future<Map<String, dynamic>> parseResponse(response) async {
  if (response.statusCode == 200) {
    if (globals.debug) {
      print("Response: " + jsonDecode(response.body).toString());
    }

    return jsonDecode(response.body);
  } else {
    throw Exception("Error on statusCode response");
  }
}

String parseDateShort(String dateToParse) {
  return dateToParse.substring(11, 16);
}
