part of api_library;

// Get the current information about venues, checkpoints and ticketTypes.
///
/// {@category EventInformation}
/// {@subCategory Information displays}
Future<List<DDEntry>> syncAllData() async {
  //160
  print('Synch events, checkpoints and venues');
  await getVenues(); // Fetches all venues
  await getCheckpoints(); // Fetches all checkpoints
  await getTicketTypes(); // Fetches all checkpoints
  List<DDEntry> events = [];

  List<Event> event = await getEvents();
  for (var v in event) {
    DDEntry a = DDEntry(key: v.id.toString(), value: v.name);
    events.add(a);
  }
  return events;
}

// Get the current information about venues.
///
/// {@category VenuesInformation}
/// {@subCategory Information displays}
Future getVenues() async {
  Map<String, dynamic> jsonData =
      await fetchAPIDataPOST("/sync/venues", {}, 'main');

  // ignore: unnecessary_null_comparison
  if (jsonData != null) {
    for (var v in jsonData['venues']) {
      Venue venue = new Venue(
        id: v['id'],
        name: v['name'],
      );
      //Checking if venue exists in local db
      if (!await VenuesDB.db.getVenueName(venue.name)) {
        await VenuesDB.db.newVenue(venue);
      }
    }
  }
}

// Get all information about checkpoints.
///
/// {@category CheckpointInformation}
/// {@subCategory Information displays}
Future getCheckpoints() async {
  Map<String, dynamic> jsonData =
      await fetchAPIDataPOST("/sync/checkpoints", {}, 'main');

  // ignore: unnecessary_null_comparison
  bool check = jsonData == null;

  print("Checkpoints recieved");

  if (!check) {
    for (var e in jsonData['checkpoints']) {
      print(e);
      var parent = e['parent_checkpoint'] == null ? 0 : e['parent_checkpoint'];
      Checkpoint check = new Checkpoint(
        id: e['id'],
        name: e['name'],
        parent: parent,
        idVenue: e['venue_id'],
        type: e['type'],
      );

      //CHECK if exist this checkpoint
      bool checkpoint = await CheckpointsDB.db.getCheckpointName(check.name);
      if (!checkpoint) {
        await CheckpointsDB.db.newCheckpoint(check);
      } else {
        await CheckpointsDB.db.updateCheckpoint(check);
      }
    }
  }
}

// Get all information about ticket Types.
///
/// {@category TicketTypesInformation}
/// {@subCategory Information displays}
Future getTicketTypes() async {
  Map<String, dynamic> jsonData =
      await fetchAPIDataPOST("/sync/ticket_types", {}, 'main');

  // ignore: unnecessary_null_comparison
  bool check = jsonData == null;

  print("ticketTypes recieved");

  if (!check) {
    //delete all to prevent bugs
    await TicketTypesDB.db.deleteAll();
    for (var e in jsonData['ticket_types']) {
      print(e);
      TicketType ticket = new TicketType(
        id: e['id'],
        name: e['name'],
        price: e['price'].toString(),
        color: e['color'],
        venueId: e['venue_id'],
      );
      await TicketTypesDB.db.newTicketTypes(ticket);
    }
  }
}

// Get all information about ticket Events
/// {@category TicketTypesInformation}
/// {@subCategory Information displays}
Future<List<Event>> getEvents() async {
  Map<String, dynamic> jsonData =
      await fetchAPIDataPOST("/sync/events", {}, 'main');

  List<Event> events = [];

  // ignore: unnecessary_null_comparison
  if (jsonData == null) {
    Event event = new Event(0, '', '');
    events.add(event);
  } else {
    for (var e in jsonData['events']) {
      DateTime dateTime = DateTime.parse(e['date']);

      Event event = new Event(
        e['id'],
        e['name'],
        dateTime.toString(),
      );
      events.add(event);
    }
  }

  return events;
}

// Configuration information PDA
/// {@category Config}
/// {@subCategory Information displays}
Future<ResponseModel> updateConfig(int idVenue, int idCheck, String type,
    {required int idEvent}) async {
  print('update/create Configuration');
  try {
    Venue? venue = await VenuesDB.db.getVenue(idVenue);
    Checkpoint? checkpoint = await CheckpointsDB.db.getCheckpoint(idCheck);

    if (venue == null || checkpoint == null) {
      return ResponseModel(error: true, message: "No hay Puntos de accesos");
    }

    Configuration? config = await ConfigurationDB.db.getConfig();

    if (config == null) {
      Configuration newC = Configuration(
          id: 0,
          venueId: venue.id,
          venueName: venue.name,
          checkpointID: checkpoint.id,
          checkpointName: checkpoint.name,
          checkpointParent: checkpoint.parent,
          syncTime: 5,
          idEvent: idEvent,
          type: type);
      await ConfigurationDB.db.newConfiguration(newC);
      globals.conf = newC;
      globals.check = checkpoint;
      globals.venue = venue;
    } else {
      config.venueId = venue.id;
      config.venueName = venue.name;
      config.checkpointID = checkpoint.id;
      config.checkpointName = checkpoint.name;
      config.checkpointParent = checkpoint.parent;
      config.syncTime = 5;
      config.idEvent = idEvent;
      config.type = type;

      globals.conf = config;
      globals.check = checkpoint;
      globals.venue = venue;

      await ConfigurationDB.db.updateConfiguration(config);
    }

    if (globals.type == 'tickets') {
      print('option tickets');
      //option tickets
      await getTicketsCheckpoint(); // Fetches all tickets by checkpoint
      await getAccreditation(); //all accreditacions by idEvent
    } else {
      print('option access');
      //option access
      await getAccess(); // Fetches all access by venue
    }
    return ResponseModel(
        error: false, message: "Configuración realizada con éxito");
  } catch (e) {
    print(e);
    return ResponseModel(
        error: true, message: "Hubo un error al configurar el dispositivo");
  }
}
