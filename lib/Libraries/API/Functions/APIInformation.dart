part of api_library;

///Get info about device
/// {@category device information}
/// {@subCategory Information displays}
Future<ResponseModel> sendInfoDevice(String status) async {
  // Get current battery level
  var _battery = Battery();
  var battery = await _battery.batteryLevel;

  // Uploading battery level
  Map<String, dynamic> jsonData = await fetchAPIDataPOST(
      "/pda/checkpoint/battery",
      {
        'deviceId': await getDeviceId(),
        'eventId': globals.conf!.idEvent.toString(),
        'checkpointId': globals.conf!.checkpointID.toString(),
        'status': status,
        'battery': battery.toString(),
      },
      'qrserver');
  // ignore: unnecessary_null_comparison
  if (jsonData == null) {
    return ResponseModel(
        error: true,
        message:
            "No hay conexión a internet, no se puede enviar el status del dispositivo");
  } else {
    return ResponseModel(
        error: jsonData['error'], message: jsonData['message']);
  }
}

/// Sync alerts
/// {@category alerts information}
/// {@subCategory Information displays}
Future<void> syncAlerts() async {
  List<Alert> alertsNotSync = await AlertDB.db.getNotSynchronizedAlert();

  var encoded = jsonEncode(alertsNotSync.toList());

  if (alertsNotSync.isNotEmpty) {
    print(jsonEncode(<String, String>{
      'data': encoded,
    }));
    Map<String, dynamic> jsonData = await fetchAPIDataPOST(
        "/pda/alert/synchronization",
        {
          'data': encoded,
        },
        'qrserver');

    if (!jsonData['error']) {
      //update all error
      for (var e in alertsNotSync) {
        e.sync = true;
        await AlertDB.db.updateAlert(e);
      }
    }
  }
}

///  Alert batery
/// {@category alerts battery information}
/// {@subCategory device information}
Future<ResponseModel> alertBattery() async {
  String _deviceId = await getDeviceId();
  var _battery = Battery();
  var battery = await _battery.batteryLevel;
  if (battery <= 15) {
    Map<String, dynamic> jsonData = await fetchAPIDataPOST(
        "/pda/alert/battery",
        {
          'checkpointID': globals.conf!.checkpointID.toString(),
          'eventID': globals.conf!.idEvent.toString(),
          'deviceID': _deviceId
        },
        'qrserver');

    // ignore: unnecessary_null_comparison
    if (jsonData == null) {
      Alert alert = new Alert(
        message: "No hay conexión a internet",
        type: 'battery',
        sync: false,
        error: false,
        checkpointId: globals.conf!.checkpointID,
        eventId: globals.conf!.idEvent,
        deviceId: await getDeviceId(),
        id: 0,
      );
      await AlertDB.db.newAlert(alert);
      return ResponseModel(
          error: true,
          message:
              "No hay conexión a internet, la alerta se enviará en breve cuando se reconecte el servicio");
    } else {
      return ResponseModel(
          error: jsonData['error'], message: jsonData['message']);
    }
  } else {
    return ResponseModel(
        error: false, message: "La bateria aun tiene un porcentaje alto");
  }
}

/// Sync errors readings
/// {@category error information}
/// {@subCategory Information displays}
Future<void> syncErrors() async {
  List<ErrorModel> errorNotSync = await ErrorDB.db.getNotSynchronizedErrors();

  var encoded = jsonEncode(errorNotSync.toList());

  if (errorNotSync.isNotEmpty) {
    print(jsonEncode(<String, String>{
      'data': encoded,
    }));
    Map<String, dynamic> jsonData = await fetchAPIDataPOST(
        "/pda/errors/synchronization",
        {
          'data': encoded,
        },
        'qrserver');

    if (!jsonData['error']) {
      //update all error
      for (var e in errorNotSync) {
        e.sync = true;
        await ErrorDB.db.updateErrors(e);
      }
    }
  }
}

/// Send alert
/// {@category alert information}
/// {@subCategory Information displays}
Future<ResponseModel> sendAlert() async {
  String _deviceId = await getDeviceId();

  Map<String, dynamic> jsonData = await fetchAPIDataPOST(
      "/pda/alert/send",
      {
        'checkpointID': globals.conf!.checkpointID.toString(),
        'eventID': globals.conf!.idEvent.toString(),
        'deviceID': _deviceId,
        'venueID': globals.conf!.venueId.toString(),
      },
      'qrserver');

  if (jsonData == null) {
    Alert alert = new Alert(
      message: "No hay conexión a internet",
      type: 'alert',
      sync: false,
      error: false,
      checkpointId: globals.conf!.checkpointID,
      eventId: globals.conf!.idEvent,
      deviceId: await getDeviceId(),
      id: 0,
    );
    await AlertDB.db.newAlert(alert);
    return ResponseModel(
        error: true,
        message:
            "No hay conexión a internet, la alerta se enviará en breve cuando se reconecte el servicio");
  } else {
    return ResponseModel(
        error: jsonData['error'], message: jsonData['message']);
  }
}
