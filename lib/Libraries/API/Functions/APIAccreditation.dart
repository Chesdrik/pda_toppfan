part of api_library;

// Get all information about accreditations by eventif.
///
/// {@category CheckpointInformation}
/// {@subCategory Information displays}
Future<void> getAccreditation() async {
  // Cleaning checkpoint db

  await AccreditationDB.db.deleteAll();

  // Fetching data
  Map<String, dynamic> jsonData = await fetchAPIDataPOST("/accreditations/sync",
      {'event_id': globals.conf!.idEvent.toString()}, 'qrserver');

  List<Accreditation> accreditation = [];

  if (jsonData['accreditations'] == null) {
    Accreditation ac = new Accreditation(
        id: 0,
        accreditationId: 0,
        eventId: 0,
        ticketTypeId: 0,
        version: 0,
        name: 'empty',
        checkpoints: []);
    accreditation.add(ac);
  } else {
    for (var e in jsonData['accreditations']) {
      List<int> checkpoint = [];
      for (var c in e['checkpoints']) {
        checkpoint.add(c);
      }
      Accreditation accr = new Accreditation(
          accreditationId: e['accreditation_id'],
          eventId: e['event_id'],
          ticketTypeId: e['ticket_type_id'],
          version: e['version'],
          name: e['name'],
          checkpoints: checkpoint,
          id: 0);

      print(accr.toMap());

      await AccreditationDB.db.newAccreditation(accr);
      accreditation.add(accr);
    }
  }
}

/// Sync all information about accreditations with synch in false.
///
/// {@category CheckpointInformation}
/// {@subCategory Information displays}
Future<void> accreditationSynchronization() async {
  List<AccreditationReading> acNotSync =
      await AccreditationReadingDB.db.getNotSynchronizedAccreditationReading();

  var encoded = jsonEncode(acNotSync.toList());
  String deviceId = await getDeviceId();

  print(jsonEncode(<String, String>{
    'data': encoded,
    'checkpointID': globals.conf!.checkpointID.toString(),
    'deviceId': deviceId
  }));

  Map<String, dynamic> jsonData = await fetchAPIDataPOST(
      "/pda/accreditations/synchronization",
      {
        'data': encoded,
        'checkpointID': globals.conf!.checkpointID.toString(),
        'deviceID': deviceId
      },
      'qrserver');

  if (!jsonData['error']) {
    for (var ac in acNotSync) {
      await AccreditationReadingDB.db.deleteAccreditationReading(ac.id);
    }
  }

  if (jsonData['tickets'] != null) {
    for (var e in jsonData['tickets']) {
      //search into local db
      bool existTicket = await AccreditationReadingDB.db
          .existsAccreditationReadingAcId(e['accreditation_reading_id']);
      //update ticket_reading_id

      //before to insert, check if exits into device, if it doesn't exist, then insert
      if (!existTicket) {
        AccreditationReading ac = new AccreditationReading(
            accreditationId: e['accreditation_reading_id'],
            checkpointID: e['checkpoint_id'],
            error: (e['error'] == 0 ? false : true),
            synchronized: true,
            readingTime: e['reading_time'],
            access: (e['type'] == 'access' ? true : false),
            deviceId: e['device_id'],
            errorMessage: '',
            id: 0);
        await AccreditationReadingDB.db
            .insertAccreditationReading(ac, insert: false);
      }
    }
  }
}

/// Process scanner accreditation qr.
///
/// {@category Scanner Accreditation}
/// {@subCategory Information QR}
Future<void> processScannedAccreditationQR(
    BuildContext context, Map<String, dynamic> qr, bool _readingAccess) async {
  print('Processing accreditation');
  print(qr.toString());

  // Defining date
  var now = new DateTime.now();
  String date = DateFormat('yyyy-MM-dd HH:mm:ss').format(now).toString();

  // Access or exit
  print('Acceso: ' + _readingAccess.toString());

  // fetching Accreditation from local DB
  var accreditation = await AccreditationDB.db.getAccreditationByID(qr['aID']);
  var version;
  if (qr.containsKey('version')) {
    version = qr['version'];
  } else {
    version = "0";
  }
  final prefs = await SharedPreferences.getInstance();
  bool offline = prefs.getBool('offline') ?? true;
  print('offiline mode');
  print(offline);

  if (accreditation!.checkpoints.contains(globals.conf!.checkpointID)) {
    String deviceId = await getDeviceId();

    // Making request
    String _readingTime =
        DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now()).toString();

    String _readingHash = "123";
    // await generateReadingHash(accreditation, _readingTime);
    if (offline) {
      //modo offline activado (osea sin internet) hace la validación local directamente
      offlineAccreditation(context, accreditation, version, date,
          _readingAccess, _readingTime, deviceId, qr);
    } else {
      // si el modo offline esta desactivado verifica con el servidor
      var response;
      try {
        response = await fetchAPIDataPOST(
            '/pda/accreditations/access',
            {
              'readingTime': _readingTime,
              'accreditationId': accreditation.accreditationId.toString(),
              'checkpointId': globals.conf!.checkpointID.toString(),
              'device_id': deviceId,
              'version': version.toString(),
              'type': (_readingAccess ? 'access' : 'exit'),
            },
            'qrserver');
        print('Requesting');
        print(response);
      } catch (e) {
        print(e);
        response = null;
      }

      if (response == null) {
        // Lectura en local
        offlineAccreditation(context, accreditation, version, date,
            _readingAccess, _readingTime, deviceId, qr);
      } else {
        // Lectura en el server
        if (response['error']) {
          // Create reading error
          print('Inserting error reading');
          print(response['message']);

          // Inserting reading into db
          await AccreditationReadingDB.db.insertAccreditationReading(
              AccreditationReading(
                  id: 0,
                  accreditationId: accreditation.accreditationId,
                  checkpointID: globals.conf!.checkpointID,
                  error: true,
                  synchronized: true,
                  readingTime: _readingTime,
                  access: _readingAccess,
                  deviceId: deviceId,
                  errorMessage: response['message']));
          // playIcorrectSound();
        } else {
          // Create reading success
          print('Inserting success reading');

          // Inserting reading into db
          await AccreditationReadingDB.db
              .insertAccreditationReading(AccreditationReading(
            id: 0,
            accreditationId: accreditation.accreditationId,
            checkpointID: globals.conf!.checkpointID,
            error: false,
            synchronized: true,
            readingTime: _readingTime,
            access: _readingAccess,
            deviceId: deviceId,
            errorMessage: '',
          ));
          // playCorrectSound();
        }

        // Popping alert
        Navigator.pop(context);
      }
    }
  } else {
    print('acceso incorrecto');
    // Poping alert
    // catchError(qr['aID'], 'accreditation', 'checkpoint', qr, date);
    // Navigator.pop(context);

    // // Checking correct access
    // String _checkpointString =
    //     await generateCorrectCheckpointAccessStringForAccreditation(
    //         accreditation);

    // // Showing alert with correct checkpoint
    // showAlert(context, true, "Acceso incorrecto",
    //     "Dirigir a accesos " + _checkpointString);
    showAlert(
        context, true, "Acceso incorrecto", "La acreditación no tiene acceso");
  }
  globals.reading = true;
  return null;
}

/// Offline validation for accreditation readings
/// {@category Scanner Accreditation}
/// {@subCategory Information QR}
Future offlineAccreditation(context, accreditation, version, date,
    _readingAccess, _readingTime, deviceId, qr) async {
  //modo offline activado (osea sin internet) hace la validación local directamente
  // Lectura en local
  print('local validation, accreditation');

  // Checking if accreditation exists on local DB
  bool accreditationExist = await AccreditationDB.db
      .accreditationExistByaccreditationId(accreditation.accreditationId);

  if (accreditationExist) {
    print(accreditation.version);
    print(version);
    print(accreditation.version.runtimeType);
    print(version.runtimeType);
    if (accreditation.version == version) {
      AccreditationReading ar = new AccreditationReading(
        accreditationId: accreditation.accreditationId,
        checkpointID: globals.conf!.checkpointID,
        error: false, //check
        synchronized: false, //because is read by device
        readingTime: date,
        access: _readingAccess,
        deviceId: await getDeviceId(), id: 0, errorMessage: '',
      );
      bool response =
          await validateAccreditationAccessExitLocal(ar, _readingAccess);
      // Popping alert
    } else if (version == 0) {
      //accreditations has not version
      AccreditationReading ar = new AccreditationReading(
        id: 0,
        accreditationId: accreditation.accreditationId,
        checkpointID: globals.conf!.checkpointID,
        error: false, //check
        synchronized: false, //because is read by device
        readingTime: date,
        access: _readingAccess,
        deviceId: await getDeviceId(), errorMessage: '',
      );
      bool response =
          await validateAccreditationAccessExitLocal(ar, _readingAccess);
    } else {
      await AccreditationDB.db.insertAccreditationReading(AccreditationReading(
          id: 0,
          accreditationId: accreditation.accreditationId,
          checkpointID: globals.conf!.checkpointID,
          error: true,
          synchronized: false,
          readingTime: _readingTime,
          access: _readingAccess,
          deviceId: deviceId,
          errorMessage: 'Versión de la acreditación incorrecta'));
      // playIcorrectSound();
      catchError(qr['aID'], 'accreditation', 'version', qr, date,
          'offlineAccreditation not same version');
      // don't have the same version
      showAlert(
          context, true, "Error", "Versión de la acreditación incorrecta");
    }
    Navigator.pop(context);
  } else {
    catchError(qr['aID'], 'accreditation', 'event', qr, date,
        'offlineAccreditation not into device');
    // Does not exist in local database
    showAlert(context, true, "Error", "QR no se encuentra en el dispositivo");
    Navigator.pop(context);
  }
}

/// Offline validation for accreditation readings in local
/// {@category Scanner Accreditation}
/// {@subCategory Information QR}
Future<bool> validateAccreditationAccessExitLocal(
    AccreditationReading accreditation, bool access) async {
  bool response = false;
  try {
    if (access) {
      response = await AccreditationReadingDB.db.verifyAccess(accreditation);
    } else {
      response = await AccreditationReadingDB.db.verifyExit(accreditation);
    }
  } catch (e) {
    print(e);
  }
  // if (response) {
  //   playIcorrectSound();
  // } else {
  //   playCorrectSound();
  // }
  return response;
}
