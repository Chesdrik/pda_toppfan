part of db_library;

class VenuesDB {
  VenuesDB._privateConstructor();
  static final VenuesDB db = VenuesDB._privateConstructor();

  Database? _database;
  int _databaseVersion = 1;
  String _databaseName = "pumasVenues.db";
  String _databaseStructure = "CREATE TABLE Venues ("
      "id INTEGER PRIMARY KEY,"
      "name TEXT"
      ")";

  Future<Database?> get database async {
    // ignore: unnecessary_null_comparison
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion,
        onOpen: (db) {},
        onCreate: _onCreate,
        onUpgrade: _onUpgrade);
  }

  void _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS Venues");
      await db.execute(_databaseStructure);
    }
  }

  newVenue(Venue newVenue) async {
    final db = await database;
    //get the biggest id in the table
    // var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM Venue");
    // int id = table.first["id"];
    //insert to the table using the new id
    var raw = await db!.rawInsert(
        "INSERT INTO Venues (id, name)"
        " VALUES (?,?)",
        [
          newVenue.id,
          newVenue.name,
        ]);
    return raw;
  }

  updateVenue(Venue newVenue) async {
    final db = await database;
    var res = await db!.update("Venues", newVenue.toMap(),
        where: "id = ?", whereArgs: [newVenue.id]);
    return res;
  }

  Future<Venue?> getVenue(int id) async {
    final db = await database;
    var res = await db!.query("Venues", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? Venue.fromMap(res.first) : null;
  }

  Future<List<Venue>> getAllVenues() async {
    final db = await database;
    var res = await db!.query("Venues");
    List<Venue> list =
        res.isNotEmpty ? res.map((c) => Venue.fromMap(c)).toList() : [];
    return list;
  }

  deleteVenue(int id) async {
    final db = await database;
    return db!.delete("Venues", where: "id = ?", whereArgs: [id]);
  }

  deleteAll() async {
    final db = await database;
    db!.rawDelete("DELETE FROM Venues");
  }

  getVenueName(String name) async {
    final db = await database;
    var res = await db!.query("Venues", where: "name = ?", whereArgs: [name]);
    return res.isNotEmpty ? true : false;
  }

  Future<List<DDEntry>> getEntryVenue() async {
    final db = await database;
    var res = await db!.query("Venues");
    List<Venue> list =
        res.isNotEmpty ? res.map((c) => Venue.fromMap(c)).toList() : [];
    List<DDEntry> venues = [];
    for (var v in list) {
      DDEntry a = DDEntry(key: v.id.toString(), value: v.name);
      venues.add(a);
    }
    return venues;
  }
}
