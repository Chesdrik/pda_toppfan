part of db_library;

class AccessDB {
  AccessDB._();

  static final AccessDB db = AccessDB._();

  Database? _database;
  int _databaseVersion = 1;
  String _databaseName = "pumasAccess.db";
  String _databaseStructure = "CREATE TABLE Access ("
      "id INTEGER PRIMARY KEY AUTOINCREMENT,"
      "idAccess INTEGER,"
      "userId INTEGER,"
      "venueId INTEGER,"
      "visitorName TEXT,"
      "start TEXT,"
      "end TEXT,"
      "type TEXT"
      ")";

  Future<Database?> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Init db
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
        version: _databaseVersion,
        onOpen: (db) {},
        onCreate: _onCreate,
        onUpgrade: _onUpgrade);
  }

  // _onCreate Script
  void _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  // _onUpgrade Script
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS AccreditationReading");
      await db.execute(_databaseStructure);
    }
  }

  newAccess(Access newItem) async {
    final db = await database;
    // Inserting Access in table
    var insertQuery = await db!.rawInsert(
        "INSERT INTO Access (idAccess, userId, venueId, visitorName, start, end, type)"
        " VALUES (?,?,?,?,?,?,?)",
        [
          newItem.idAccess,
          newItem.userId,
          newItem.venueId,
          newItem.visitorName,
          newItem.start,
          newItem.end,
          newItem.type
        ]);
    return insertQuery;
  }

  updateConfiguration(Access access) async {
    final db = await database;
    var res = await db!.update("Access", access.toMap(),
        where: "id = ?", whereArgs: [access.idAccess]);
    return res;
  }

  getAccess(int id) async {
    final db = await database;
    var res = await db!.query("Access", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? Access.fromMap(res.first) : null;
  }

  getAccessByIDAccess(int idAccess) async {
    final db = await database;
    var res =
        await db!.query("Access", where: "idAccess = ?", whereArgs: [idAccess]);
    return res.isNotEmpty ? Access.fromMap(res.first) : null;
  }

  deleteAll() async {
    final db = await database;
    db!.rawDelete("DELETE FROM Access");
  }

  Future<List<Access>> getAllAccess() async {
    print("Fetching tickets from db");
    final db = await database;
    var res = await db!.query("Access");

    List<Access> list =
        res.isNotEmpty ? res.map((c) => Access.fromMap(c)).toList() : [];

    return list;
  }

  getAccessByID(int id) async {
    final db = await database;
    var res = await db!.query("Access", where: "idAccess = ?", whereArgs: [id]);
    return res.isNotEmpty ? true : false;
  }
}
