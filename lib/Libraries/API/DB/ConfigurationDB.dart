part of db_library;

class ConfigurationDB {
  ConfigurationDB._();

  static final ConfigurationDB db = ConfigurationDB._();

  Database? _database;
  int _databaseVersion = 1;
  String _databaseName = "pumasConfigurations.db";
  String _databaseStructure = "CREATE TABLE Configurations ("
      "id INTEGER PRIMARY KEY AUTOINCREMENT,"
      "venueId INTEGER,"
      "venueName TEXT,"
      "checkpointID INTEGER,"
      "checkpointName TEXT,"
      "checkpointParent INTEGER,"
      "type TEXT,"
      "idEvent INTEGER,"
      "syncTime INTEGER"
      ")";

  Future<Database?> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Init db
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
        version: _databaseVersion,
        onOpen: (db) {},
        onCreate: _onCreate,
        onUpgrade: _onUpgrade);
  }

  // _onCreate Script
  void _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  // _onUpgrade Script
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS Configurations");
      await db.execute(_databaseStructure);
    }
  }

  newConfiguration(Configuration newItem) async {
    final db = await database;
    // Inserting Configuration in table
    var insertQuery = await db!.rawInsert(
        "INSERT INTO Configurations (venueId, venueName, checkpointID, checkpointName, checkpointParent, type, idEvent, syncTime)"
        " VALUES (?,?,?,?,?,?,?,?)",
        [
          newItem.venueId,
          newItem.venueName,
          newItem.checkpointID,
          newItem.checkpointName,
          newItem.checkpointParent,
          newItem.type,
          newItem.idEvent,
          newItem.syncTime
        ]);
    return insertQuery;
  }

  updateConfiguration(Configuration configuration) async {
    final db = await database;
    var res = await db!.update("Configurations", configuration.toMap(),
        where: "id = ?", whereArgs: [configuration.id]);
    return res;
  }

  getConfiguration(int id) async {
    final db = await database;
    var res =
        await db!.query("Configurations", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? Configuration.fromMap(res.first) : null;
  }

  getConfigurationByIDs(int venueId, int checkpointID) async {
    final db = await database;
    var res = await db!.query("Configurations",
        where: "venueId = ? AND checkpointID = ?",
        whereArgs: [venueId, checkpointID]);
    return res.isNotEmpty ? Configuration.fromMap(res.first) : null;
  }

  deleteAll() async {
    final db = await database;
    db!.rawDelete("DELETE FROM Configurations");
  }

  Future<List<Configuration>> getAllConfigurations() async {
    final db = await database;
    var res = await db!.query("Configurations");
    List<Configuration> list =
        res.isNotEmpty ? res.map((c) => Configuration.fromMap(c)).toList() : [];
    return list;
  }

  getExitConfig(Configuration config) async {
    final db = await database;
    var res = await db!.query("Configurations",
        where: "venueId = ? AND checkpointID=?",
        whereArgs: [config.venueId, config.checkpointID]);
    return res.isNotEmpty ? true : false;
  }

  Future<Configuration?> getConfig() async {
    final db = await database;
    var res = await db!
        .rawQuery("SELECT * FROM Configurations ORDER BY id ASC LIMIT 1");
    print(res);

    return res.isNotEmpty ? Configuration.fromMap(res.first) : null;
  }
}
