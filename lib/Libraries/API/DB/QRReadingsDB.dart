part of db_library;

class QRReadingsDB {
  QRReadingsDB._();

  static final QRReadingsDB db = QRReadingsDB._();

  Database? _database;
  int _databaseVersion = 2;
  String _databaseName = "pumasQRReadings.db";
  String _databaseStructure = "CREATE TABLE QRReadings ("
      "id INTEGER PRIMARY KEY AUTOINCREMENT,"
      "readingType TEXT,"
      "checkpointId INTEGER,"
      "readingId INTEGER,"
      "error BIT,"
      "readingTime,"
      "access BIT,"
      "deviceId TEXT,"
      "errorMessage TEXT"
      ")";

  Future<Database?> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Init db
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
        version: _databaseVersion,
        onOpen: (db) {},
        onCreate: _onCreate,
        onUpgrade: _onUpgrade);
  }

  // _onCreate Script
  void _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  // _onUpgrade Script
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS QRReadings");
      await db.execute(_databaseStructure);
    }
  }

  Future<void> insertQRReading(QRReading QRReading) async {
    final db = await database;
    print('Inserting QRReading');

    // Inserting ticket reading in table
    // ignore: unused_local_variable
    var insertQuery = await db!.rawInsert(
        "INSERT INTO QRReadings (readingType, checkpointId, readingId, error, readingTime, access, deviceId, errorMessage)"
        " VALUES (?,?,?,?,?,?,?,?)",
        [
          QRReading.readingType,
          QRReading.checkpointId,
          QRReading.readingId,
          QRReading.error,
          QRReading.readingTime,
          QRReading.access,
          QRReading.deviceId,
          QRReading.errorMessage,
        ]);
  }

  // Get all accreditation readings
  Future<List<QRReading>> getAllQRReadings() async {
    print("Fetching QRReadings from db");
    final db = await database;
    var res = await db!.query("QRReadings ORDER BY readingTime DESC");

    List<QRReading> list =
        res.isNotEmpty ? res.map((c) => QRReading.fromMap(c)).toList() : [];

    print(list.toString());

    return list;
  }

  // Delete all records
  deleteAll() async {
    final db = await database;
    db!.rawDelete("DELETE FROM QRReadings");

    return true;
  }

  // Delete all accreditation records
  Future<bool> deleteAllAccreditationReadings() async {
    final db = await database;
    // db.rawDelete("DELETE FROM QRReadings WHERE readingType = 'accreditation'");
    db!.delete("QRReadings",
        where: "readingType = ? ", whereArgs: ['accreditation']);

    print('Deleting all accreditation readings from QRReadings');

    return true;
  }

  // Delete all records
  Future<bool> deleteAllTicketReadings() async {
    final db = await database;
    // db.rawDelete("DELETE FROM QRReadings WHERE readingType = 'ticket'");
    db!.delete("QRReadings", where: "readingType = ? ", whereArgs: ['ticket']);
    print('Deleting all ticket readings from QRReadings');

    return true;
  }
}
