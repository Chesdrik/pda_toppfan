library db_library;

// Required imports
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pda_toppfan/Models/Access.dart';
import 'package:pda_toppfan/Models/Accreditation.dart';
import 'package:pda_toppfan/Models/AccreditationReading.dart';
import 'package:pda_toppfan/Models/Alert.dart';
import 'package:pda_toppfan/Models/Checkpoint.dart';
import 'package:pda_toppfan/Models/Configuration.dart';
import 'package:pda_toppfan/Models/DDEntry.dart';
import 'package:pda_toppfan/Models/ErrorModel.dart';
import 'package:pda_toppfan/Models/QRReading.dart';
import 'package:pda_toppfan/Models/RequirementAccess.dart';
import 'package:pda_toppfan/Models/Ticket.dart';
import 'package:pda_toppfan/Models/TicketReading.dart';
import 'package:pda_toppfan/Models/TicketType.dart';
import 'package:pda_toppfan/Models/Venue.dart';
import 'package:sqflite/sqflite.dart';
import 'package:pda_toppfan/Libraries/GlobalData/GlobalLibrary.dart';
import 'package:pda_toppfan/Libraries/Helpers/HelpersLibrary.dart';

//parts
part 'package:pda_toppfan/Libraries/API/DB/VenuesDB.dart';
part 'package:pda_toppfan/Libraries/API/DB/CheckpointDB.dart';
part 'package:pda_toppfan/Libraries/API/DB/TicketTypesDB.dart';
part 'package:pda_toppfan/Libraries/API/DB/ConfigurationDB.dart';
part 'package:pda_toppfan/Libraries/API/DB/TicketDB.dart';
part 'package:pda_toppfan/Libraries/API/DB/AccreditationDB.dart';
part 'package:pda_toppfan/Libraries/API/DB/AccessDB.dart';
part 'package:pda_toppfan/Libraries/API/DB/AccessRequirementDB.dart';
part 'package:pda_toppfan/Libraries/API/DB/TicketReadingsDB.dart';
part 'package:pda_toppfan/Libraries/API/DB/QRReadingsDB.dart';
part 'package:pda_toppfan/Libraries/API/DB/AccreditationReadingDB.dart';
part 'package:pda_toppfan/Libraries/API/DB/AlertDB.dart';
part 'package:pda_toppfan/Libraries/API/DB/ErrorDB.dart';
