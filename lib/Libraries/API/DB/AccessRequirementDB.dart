part of db_library;

class AccessRequirementsDB {
  AccessRequirementsDB._();

  static final AccessRequirementsDB db = AccessRequirementsDB._();

  Database? _database;
  int _databaseVersion = 1;
  String _databaseName = "pumasAccessRequirements.db";
  String _databaseStructure = "CREATE TABLE AccessRequirements ("
      "id INTEGER PRIMARY KEY AUTOINCREMENT,"
      "idAccess INTEGER,"
      "accessRequirementId INTEGER,"
      "type TEXT,"
      "request TEXT,"
      "dataType TEXT,"
      "status TEXT"
      ")";

  Future<Database?> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Init db
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
        version: _databaseVersion,
        onOpen: (db) {},
        onCreate: _onCreate,
        onUpgrade: _onUpgrade);
  }

  // _onCreate Script
  void _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  // _onUpgrade Script
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS AccessRequirements");
      await db.execute(_databaseStructure);
    }
  }

  newRequirementAccess(RequirementAccess newItem) async {
    final db = await database;
    // Inserting Configuration in table
    var insertQuery = await db!.rawInsert(
        "INSERT INTO AccessRequirements (idAccess, accessRequirementId, type, request, dataType, status)"
        " VALUES (?,?,?,?,?,?)",
        [
          newItem.idAccess,
          newItem.accessRequirementId,
          newItem.type,
          newItem.request,
          newItem.dataType,
          newItem.status,
        ]);
    return insertQuery;
  }

  updateRequirementAccess(RequirementAccess requirementAccess) async {
    final db = await database;
    var res = await db!.update("AccessRequirements", requirementAccess.toMap(),
        where: "id = ?", whereArgs: [requirementAccess.id]);
    return res;
  }

  getRequirementAccess(int id) async {
    final db = await database;
    var res =
        await db!.query("AccessRequirements", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? RequirementAccess.fromMap(res.first) : null;
  }

  getRequirementAccessByIDAccess(int idAccess) async {
    final db = await database;
    var res = await db!.query("AccessRequirements",
        where: "idAccess = ?", whereArgs: [idAccess]);
    return res.isNotEmpty ? RequirementAccess.fromMap(res.first) : null;
  }

  deleteAll() async {
    final db = await database;
    db!.rawDelete("DELETE FROM AccessRequirements");
  }

  Future<List<RequirementAccess>> getAllRequirementAccess() async {
    print("Fetching tickets from db");
    final db = await database;
    var res = await db!.query("AccessRequirements");

    List<RequirementAccess> list = res.isNotEmpty
        ? res.map((c) => RequirementAccess.fromMap(c)).toList()
        : [];

    print(list.toString());

    return list;
  }

  getRequirementAccessByID(int id) async {
    final db = await database;
    var res = await db!
        .query("RequirementAccess", where: "idAccess = ?", whereArgs: [id]);
    return res.isNotEmpty ? true : false;
  }

  Future<List<RequirementAccess>> getAllRequirementAccessForidAccess(
      int id) async {
    print("Fetching ticket readings from db");
    final db = await database;
    var res = await db!
        .query("AccessRequirements", where: "idAccess = ?", whereArgs: [id]);

    List<RequirementAccess> list = res.isNotEmpty
        ? res.map((c) => RequirementAccess.fromMap(c)).toList()
        : [];
    return list;
  }
}
