part of db_library;

class AccreditationReadingDB {
  AccreditationReadingDB._();

  static final AccreditationReadingDB db = AccreditationReadingDB._();

  Database? _database;
  int _databaseVersion = 1;
  String _databaseName = "pumasAccreditationReading.db";
  String _databaseStructure = "CREATE TABLE AccreditationReading ("
      "id INTEGER PRIMARY KEY AUTOINCREMENT,"
      "accreditationId INTEGER,"
      "checkpointID INTEGER,"
      "error BIT,"
      "synchronized BIT,"
      "readingTime,"
      "access BIT,"
      "deviceId TEXT,"
      "errorMessage TEXT"
      ")";

  Future<Database?> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Init db
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
        version: _databaseVersion,
        onOpen: (db) {},
        onCreate: _onCreate,
        onUpgrade: _onUpgrade);
  }

  // _onCreate Script
  void _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  // _onUpgrade Script
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS AccreditationReading");
      await db.execute(_databaseStructure);
    }
  }

  Future<void> getTableStructure() async {
    final db = await database;
    var returneddata =
        await db!.rawQuery("PRAGMA table_info(AccreditationReading)");

    print('getTableStructure');
    print(returneddata);
  }

  Future<void> insertAccreditationReading(
      AccreditationReading accreditationReading,
      {bool insert = true}) async {
    final db = await database;
    print('Inserting');

    // Inserting ticket reading in table
    int insertId = await db!.rawInsert(
        "INSERT INTO AccreditationReading (accreditationId, checkpointID, error, synchronized, readingTime, access, deviceId, errorMessage)"
        " VALUES (?,?,?,?,?,?,?,?)",
        [
          accreditationReading.accreditationId,
          accreditationReading.checkpointID,
          accreditationReading.error,
          accreditationReading.synchronized,
          accreditationReading.readingTime,
          accreditationReading.access,
          accreditationReading.deviceId,
          accreditationReading.errorMessage
        ]);

    if (insert) {
      // Inserting reading in intermediate table
      await QRReadingsDB.db.insertQRReading(QRReading(
          id: 0,
          readingType: 'accreditation',
          checkpointId: accreditationReading.checkpointID,
          readingId: insertId,
          error: accreditationReading.error,
          readingTime: accreditationReading.readingTime,
          access: accreditationReading.access,
          deviceId: accreditationReading.deviceId,
          errorMessage: accreditationReading.errorMessage));
    }
  }

  // Update accreditation reading
  updateAccreditationReading(
      AccreditationReading newAccreditationReading) async {
    final db = await database;
    var res = await db!.update(
        "AccreditationReading", newAccreditationReading.toMap(),
        where: "id = ?", whereArgs: [newAccreditationReading.id]);
    return res;
  }

  // Counting total access for accreditationId in device checkpointID
  Future<int?> totalAccessForAccreditation(int accreditationId) async {
    // Fetching db
    final db = await database;

    // Total access query
    int? totalAccess = Sqflite.firstIntValue(await db!.rawQuery(
        "SELECT COUNT (*) from  AccreditationReading WHERE accreditationId=? AND checkpointID=? AND access=1 AND error=0",
        [accreditationId, conf!.checkpointID]));

    print(totalAccess.toString() +
        " access found for accreditationId=" +
        accreditationId.toString() +
        " AND checkpointID=" +
        conf!.checkpointID.toString());

    return totalAccess;
  }

  // Counting total exits for accreditationId in device checkpointID
  Future<int?> totalExitsForAccreditation(int accreditationId) async {
    // Fetching db
    final db = await database;

    // Total exits query
    int? totalExits = Sqflite.firstIntValue(await db!.rawQuery(
        "SELECT COUNT (*) from  AccreditationReading WHERE accreditationId=? AND checkpointID=? AND access=0 AND error=0",
        [accreditationId, conf!.checkpointID]));

    print(totalExits.toString() +
        " exits found for accreditationId=" +
        accreditationId.toString() +
        " AND checkpointID=" +
        conf!.checkpointID.toString());

    return totalExits;
  }

  // Verifying accreditation has access to event
  // TODO validate access with all checkpoints in same group
  Future<bool> verifyAccess(AccreditationReading accreditationReading) async {
    print('verifyAccess');

    int? access =
        await totalAccessForAccreditation(accreditationReading.accreditationId);
    int? outs =
        await totalExitsForAccreditation(accreditationReading.accreditationId);

    if (access! - outs! == 0) {
      // Access authorized
      print('Access authorized');
      accreditationReading.error = false;
    } else if (access > outs) {
      // Access denied
      print('Access denied');
      accreditationReading.error = true;
    } else if (access - outs < 0) {
      // Error no te salgas mas veces
      print('Access denied');
      accreditationReading.error = true;
    }

    // Inserting reading into db
    await insertAccreditationReading(accreditationReading);
    return accreditationReading.error;
  }

  // Verifying if orderID-ticketID has out permission with defined amount
  Future<bool> verifyExit(AccreditationReading accreditationReading) async {
    int? access =
        await totalAccessForAccreditation(accreditationReading.accreditationId);
    int? outs =
        await totalExitsForAccreditation(accreditationReading.accreditationId);

    print("verifyExit");
    print("Access: " + access.toString());
    print("Exit: " + outs.toString());

    if (access! > outs!) {
      // Permitido salida
      // Create reading success
      print('Inserting success reading');

      // Inserting reading into db
      accreditationReading.error = false;
      await insertAccreditationReading(accreditationReading);
    } else if (access <= outs) {
      // No puede, registar error
      // Inserting reading into db
      accreditationReading.error = true;
      await insertAccreditationReading(accreditationReading);
    }

    return accreditationReading.error;
  }

  // Get accreditation reading by id
  Future<AccreditationReading?> getAccreditationReading(int id) async {
    final db = await database;
    var res = await db!
        .query("AccreditationReading", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? AccreditationReading.fromMap(res.first) : null;
  }

  // Get all accreditation readings
  Future<List<AccreditationReading>> getAllAccreditationReadings() async {
    print("Fetching accreditation readings from db");
    final db = await database;
    var res = await db!.query("AccreditationReading");

    print('Response');
    print(res.toString());

    List<AccreditationReading> list = res.isNotEmpty
        ? res.map((c) => AccreditationReading.fromMap(c)).toList()
        : [];

    print(list.toString());

    return list;
  }

  Future<List<AccreditationReading>>
      getAllAccreditationReadingsForCheckpoint() async {
    // Fetching device id
    String _deviceId = await getDeviceId();

    print("Fetching accreditation readings from db for checkpoint " +
        conf!.checkpointID.toString() +
        " and deviceId = " +
        _deviceId);
    final db = await database;
    var res = await db!.query("AccreditationReading",
        where: "checkpointID = ? AND deviceId=? ORDER BY readingTime DESC",
        whereArgs: [conf!.checkpointID, _deviceId]);

    List<AccreditationReading> list = res.isNotEmpty
        ? res.map((c) => AccreditationReading.fromMap(c)).toList()
        : [];

    print(list.toString());

    return list;
  }

  // Delete ticket reading by id
  Future deleteAccreditationReading(int id) async {
    final db = await database;
    db!.delete("AccreditationReading", where: "id = ?", whereArgs: [id]);
  }

  // Delete accreditation reading by checkpoint
  Future deleteAccreditationReadingForCheckpoint(int accreditationId) async {
    final db = await database;

    db!.delete("AccreditationReading",
        where: "accreditationId = ? AND checkpointID=?",
        whereArgs: [accreditationId, conf!.checkpointID]);
  }

  // Delete all records
  Future deleteAll() async {
    final db = await database;
    db!.rawDelete("DELETE FROM AccreditationReading");

    // Delete all accreditation readings from QRReading db
    print("About to delete accreditation readings from QRSERVER");
    await QRReadingsDB.db.deleteAllAccreditationReadings();
  }

  // Get accreditation readings with synchronized in false
  Future<List<AccreditationReading>>
      getNotSynchronizedAccreditationReading() async {
    final db = await database;
    var res = await db!.query("AccreditationReading",
        where: "synchronized = ?", whereArgs: [0]);
    List<AccreditationReading> list = res.isNotEmpty
        ? res.map((c) => AccreditationReading.fromMap(c)).toList()
        : [];
    return list;
  }

  ///get tickets readings with synchronized in true
  Future<List<AccreditationReading>>
      getSynchronizedAccreditationReading() async {
    final db = await database;
    var res = await db!.query("AccreditationReading",
        where: "synchronized = ? ", whereArgs: [1]);

    List<AccreditationReading> list = res.isNotEmpty
        ? res.map((c) => AccreditationReading.fromMap(c)).toList()
        : [];
    return list;
  }

  // get bool accreditation reading by id from db qrserver
  existsAccreditationReadingAcId(int id) async {
    final db = await database;
    var res = await db!.query("AccreditationReading",
        where: "accreditationId = ?", whereArgs: [id]);
    return res.isNotEmpty ? true : false;
  }
}
