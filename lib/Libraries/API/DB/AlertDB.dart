part of db_library;

class AlertDB {
  AlertDB._();

  static final AlertDB db = AlertDB._();

  Database? _database;
  int _databaseVersion = 3;
  String _databaseName = "pumasAlerts.db";
  String _databaseStructure = "CREATE TABLE Alerts ("
      "id INTEGER PRIMARY KEY AUTOINCREMENT,"
      "message INTEGER,"
      "type INTEGER,"
      "sync BIT,"
      "error BIT,"
      "checkpointId INTEGER,"
      "eventId INTEGER,"
      "deviceId TEXT"
      ")";

  Future<Database?> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Init db
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
        version: _databaseVersion,
        onOpen: (db) {},
        onCreate: _onCreate,
        onUpgrade: _onUpgrade);
  }

  // _onCreate Script
  void _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  // _onUpgrade Script
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS Alerts");
      await db.execute(_databaseStructure);
    }
  }

  newAlert(Alert newItem) async {
    final db = await database;
    // Inserting Alert in table
    var insertQuery = await db!.rawInsert(
        "INSERT INTO Alerts (message, type, sync, error,checkpointId, eventId, deviceId)"
        " VALUES (?,?,?,?,?,?,?)",
        [
          newItem.message,
          newItem.type,
          newItem.sync,
          newItem.error,
          newItem.checkpointId,
          newItem.eventId,
          newItem.deviceId
        ]);
    print("Inserting Alert in table");
    return insertQuery;
  }

  updateAlert(Alert alert) async {
    final db = await database;
    var res = await db!.update("Alerts", alert.toMap(),
        where: "id = ?", whereArgs: [alert.id]);
    return res;
  }

  getAlert(int id) async {
    final db = await database;
    var res = await db!.query("Alerts", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? Alert.fromMap(res.first) : null;
  }

  deleteAll() async {
    final db = await database;
    db!.rawDelete("DELETE FROM Alerts");
  }

  Future<List<Alert>> getAllAlert() async {
    print("Fetching tickets from db");
    final db = await database;
    var res = await db!.query("Alerts");

    List<Alert> list =
        res.isNotEmpty ? res.map((c) => Alert.fromMap(c)).toList() : [];

    print(list.toString());

    return list;
  }

  // Get accreditation readings with synchronized in false
  Future<List<Alert>> getNotSynchronizedAlert() async {
    final db = await database;
    var res = await db!.query("Alerts", where: "sync = ?", whereArgs: [0]);
    List<Alert> list =
        res.isNotEmpty ? res.map((c) => Alert.fromMap(c)).toList() : [];
    return list;
  }
}
