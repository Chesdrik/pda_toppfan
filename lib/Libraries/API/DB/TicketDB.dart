part of db_library;

class TicketDB {
  TicketDB._();

  static final TicketDB db = TicketDB._();

  Database? _database;
  int _databaseVersion = 1;
  String _databaseName = "pumasTickets.db";
  String _databaseStructure = "CREATE TABLE Tickets ("
      "id INTEGER PRIMARY KEY AUTOINCREMENT,"
      "ticketId INTEGER,"
      "eventId INTEGER,"
      "ticketTypeId INTEGER,"
      "name TEXT,"
      "orderId INTEGER,"
      "amount INTEGER,"
      "checkpoints TEXT"
      ")";

  Future<Database?> get database async {
    // ignore: unnecessary_null_comparison
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Init db
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
        version: _databaseVersion,
        onOpen: (db) {},
        onCreate: _onCreate,
        onUpgrade: _onUpgrade);
  }

  // _onCreate Script
  void _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  // _onUpgrade Script
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS Tickets");
      await db.execute(_databaseStructure);
    }
  }

  newTicket(Ticket newTicket) async {
    final db = await database;

    // Inserting ticket in table
    var insertQuery = await db!.rawInsert(
        "INSERT INTO Tickets (ticketId, eventId, ticketTypeId, name, orderId, amount, checkpoints)"
        " VALUES (?,?,?,?,?,?,?)",
        [
          newTicket.ticketId,
          newTicket.eventId,
          newTicket.ticketTypeId,
          newTicket.name,
          newTicket.orderId,
          newTicket.amount,
          newTicket.checkpoints,
        ]);
    return insertQuery;
  }

  Future<Ticket?> getTicket(int id) async {
    final db = await database;
    var response = await db!.query("Tickets", where: "id = ?", whereArgs: [id]);
    return response.isNotEmpty ? Ticket.fromMap(response.first) : null;
  }

  Future<Ticket?> getTicketByID(int ticketId) async {
    final db = await database;
    var response = await db!
        .query("Tickets", where: "ticketId = ?", whereArgs: [ticketId]);

    return response.isNotEmpty ? Ticket.fromMap(response.first) : null;
  }

  Future<bool> ticketExists(int id) async {
    final db = await database;
    var response = await db!.query("Tickets", where: "id = ?", whereArgs: [id]);
    return response.isNotEmpty ? true : false;
  }

  Future<bool> ticketExistByticketId(int ticketId) async {
    final db = await database;
    var response = await db!
        .query("Tickets", where: "ticketId = ?", whereArgs: [ticketId]);
    return response.isNotEmpty ? true : false;
  }

  Future<List<Ticket>> getAllTickets() async {
    print("Fetching tickets from db");
    final db = await database;
    var res = await db!.query("Tickets");

    List<Ticket> list =
        res.isNotEmpty ? res.map((c) => Ticket.fromMap(c)).toList() : [];

    print(list.toString());

    return list;
  }

  Future deleteTicket(int id) async {
    final db = await database;
    db!.delete("Tickets", where: "id = ?", whereArgs: [id]);
  }

  Future<void> deleteAll() async {
    final db = await database;
    db!.rawDelete("DELETE FROM Tickets");
  }
}
