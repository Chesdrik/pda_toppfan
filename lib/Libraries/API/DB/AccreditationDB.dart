part of db_library;

class AccreditationDB {
  AccreditationDB._();

  static final AccreditationDB db = AccreditationDB._();

  Database? _database;
  int _databaseVersion = 2;
  String _databaseName = "pumasAccreditations.db";
  String _databaseStructure = "CREATE TABLE Accreditations ("
      "id INTEGER PRIMARY KEY AUTOINCREMENT,"
      "accreditationId INTEGER,"
      "eventID INTEGER,"
      "ticketTypeID INTEGER,"
      "version INTEGER,"
      "name TEXT,"
      "checkpoints TEXT"
      ")";

  Future<Database?> get database async {
    // ignore: unnecessary_null_comparison
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Init db
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
        version: _databaseVersion,
        onOpen: (db) {},
        onCreate: _onCreate,
        onUpgrade: _onUpgrade);
  }

  // _onCreate Script
  void _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  // _onUpgrade Script
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS Accreditations");
      await db.execute(_databaseStructure);
    }
  }

  newAccreditation(Accreditation newAccreditation) async {
    final db = await database;
    //encode the lisof checkpoiunts to conver string
    String checkpointsString = jsonEncode(newAccreditation.checkpoints);

    // Inserting Accreditation  in table
    var insertQuery = await db!.rawInsert(
        "INSERT INTO Accreditations (accreditationId, eventID, ticketTypeID, version, name, checkpoints)"
        " VALUES (?,?,?,?,?,?)",
        [
          newAccreditation.accreditationId,
          newAccreditation.eventId,
          newAccreditation.ticketTypeId,
          newAccreditation.version,
          newAccreditation.name,
          checkpointsString
        ]);
    return insertQuery;
  }

  Future<Accreditation?> getAccreditation(int id) async {
    final db = await database;
    var response =
        await db!.query("Accreditations", where: "id = ?", whereArgs: [id]);
    return response.isNotEmpty ? Accreditation.fromMap(response.first) : null;
  }

  Future<Accreditation?> getAccreditationByID(int accreditationId) async {
    final db = await database;
    var response = await db!.query("Accreditations",
        where: "accreditationId = ?", whereArgs: [accreditationId]);

    return response.isNotEmpty ? Accreditation.fromMap(response.first) : null;
  }

  Future<Accreditation?> getAccreditationByVersion(int version) async {
    final db = await database;
    var response = await db!
        .query("Accreditations", where: "version = ?", whereArgs: [version]);

    return response.isNotEmpty ? Accreditation.fromMap(response.first) : null;
  }

  Future<bool> accreditationExist(int id) async {
    final db = await database;
    var response =
        await db!.query("Accreditations", where: "id = ?", whereArgs: [id]);
    return response.isNotEmpty ? true : false;
  }

  Future<bool> accreditationExistByaccreditationId(int accreditationId) async {
    final db = await database;
    var response = await db!.query("Accreditations",
        where: "accreditationId = ?", whereArgs: [accreditationId]);
    return response.isNotEmpty ? true : false;
  }

  Future<List<Accreditation>> getAllAccreditations() async {
    print("Fetching accreditations from db");
    final db = await database;
    var res = await db!.query("Accreditations");

    List<Accreditation> list =
        res.isNotEmpty ? res.map((c) => Accreditation.fromMap(c)).toList() : [];

    return list;
  }

  Future deleteAccreditation(int id) async {
    final db = await database;
    db!.delete("Accreditations", where: "id = ?", whereArgs: [id]);
  }

  deleteAll() async {
    final db = await database;
    db!.rawDelete("DELETE FROM Accreditations");
  }

  Future<void> insertAccreditationReading(
      AccreditationReading accreditationReading,
      {bool insert = true}) async {
    final db = await database;
    print('Inserting');

    // Inserting ticket reading in table
    int insertId = await db!.rawInsert(
        "INSERT INTO AccreditationReading (accreditationId, checkpointID, error, synchronized, readingTime, access, deviceId, errorMessage)"
        " VALUES (?,?,?,?,?,?,?,?)",
        [
          accreditationReading.accreditationId,
          accreditationReading.checkpointID,
          accreditationReading.error,
          accreditationReading.synchronized,
          accreditationReading.readingTime,
          accreditationReading.access,
          accreditationReading.deviceId,
          accreditationReading.errorMessage
        ]);

    if (insert) {
      // Inserting reading in intermediate table
      await QRReadingsDB.db.insertQRReading(QRReading(
          id: 0,
          readingType: 'accreditation',
          checkpointId: accreditationReading.checkpointID,
          readingId: insertId,
          error: accreditationReading.error,
          readingTime: accreditationReading.readingTime,
          access: accreditationReading.access,
          deviceId: accreditationReading.deviceId,
          errorMessage: accreditationReading.errorMessage));
    }
  }
}
