part of db_library;

class TicketTypesDB {
  TicketTypesDB._();

  static final TicketTypesDB db = TicketTypesDB._();

  Database? _database;
  int _databaseVersion = 1;
  String _databaseName = "pumasTicketTypes.db";
  String _databaseStructure = "CREATE TABLE TicketTypes ("
      "id INTEGER PRIMARY KEY,"
      "name TEXT,"
      "price TEXT,"
      "color TEXT,"
      "venueId INTEGER"
      ")";

  Future<Database?> get database async {
    // ignore: unnecessary_null_comparison
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Init db
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
        version: _databaseVersion,
        onOpen: (db) {},
        onCreate: _onCreate,
        onUpgrade: _onUpgrade);
  }

  // _onCreate Script
  void _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  // _onUpgrade Script
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS TicketTypes");
      await db.execute(_databaseStructure);
    }
  }

  newTicketTypes(TicketType newTicket) async {
    final db = await database;

    // Inserting ticket in table
    var insertQuery = await db!.rawInsert(
        "INSERT INTO TicketTypes (id, name, price, color, venueId)"
        " VALUES (?,?,?,?,?)",
        [
          newTicket.id,
          newTicket.name,
          newTicket.price,
          newTicket.color,
          newTicket.venueId,
        ]);
    return insertQuery;
  }

  Future<TicketType?> getTicketType(int id) async {
    final db = await database;
    var response =
        await db!.query("TicketTypes", where: "id = ?", whereArgs: [id]);
    return response.isNotEmpty ? TicketType.fromMap(response.first) : null;
  }

  Future<TicketType?> getTicketTypeByID(int id) async {
    final db = await database;
    var response =
        await db!.query("TicketTypes", where: "id = ?", whereArgs: [id]);

    return response.isNotEmpty ? TicketType.fromMap(response.first) : null;
  }

  Future<bool> ticketTypeExists(int id) async {
    final db = await database;
    var response =
        await db!.query("TicketTypes", where: "id = ?", whereArgs: [id]);
    return response.isNotEmpty ? true : false;
  }

  Future<List<TicketType>> getAllTicketTypes() async {
    final db = await database;
    var res = await db!.query("TicketTypes");
    List<TicketType> list =
        res.isNotEmpty ? res.map((c) => TicketType.fromMap(c)).toList() : [];
    return list;
  }

  Future deleteTicketType(int id) async {
    final db = await database;
    db!.delete("TicketTypes", where: "id = ?", whereArgs: [id]);
  }

  Future<void> deleteAll() async {
    final db = await database;
    db!.rawDelete("DELETE FROM TicketTypes");
  }
}
