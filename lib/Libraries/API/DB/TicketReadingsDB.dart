part of db_library;

class TicketReadingsDB {
  TicketReadingsDB._();

  static final TicketReadingsDB db = TicketReadingsDB._();

  Database? _database;
  int _databaseVersion = 3;
  String _databaseName = "pumasTicketReadings.db";
  String _databaseStructure = "CREATE TABLE TicketReadings ("
      "id INTEGER PRIMARY KEY AUTOINCREMENT,"
      "orderId INTEGER,"
      "ticketId INTEGER,"
      "ticketReadingId INTEGER,"
      "checkpointId INTEGER,"
      "error BIT,"
      "synchronized BIT,"
      "readingTime,"
      "access INTEGER,"
      "deviceId TEXT,"
      "errorMessage TEXT"
      ")";

  Future<Database?> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Init db
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
        version: _databaseVersion,
        onOpen: (db) {},
        onCreate: _onCreate,
        onUpgrade: _onUpgrade);
  }

  // _onCreate Script
  void _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  // _onUpgrade Script
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS TicketReadings");
      await db.execute(_databaseStructure);
    }
  }

  // Insert TicketReading into DB
  Future<void> insertTicketReading(TicketReading ticketReading,
      {bool insert = true}) async {
    final db = await database;

    // Inserting ticket reading in table
    int insertId = await db!.rawInsert(
        "INSERT INTO TicketReadings (orderId, ticketId, checkpointId, error, synchronized, readingTime, access, deviceId, errorMessage)"
        " VALUES (?,?,?,?,?,?,?,?,?)",
        [
          ticketReading.orderId,
          ticketReading.ticketId,
          conf!.checkpointID,
          ticketReading.error,
          ticketReading.synchronized,
          ticketReading.readingTime,
          ticketReading.access,
          ticketReading.deviceId,
          ticketReading.errorMessage,
        ]);

    if (insert) {
      // Inserting reading in intermediate table
      await QRReadingsDB.db.insertQRReading(QRReading(
        id: 0,
        readingType: 'ticket',
        checkpointId: ticketReading.checkpointId,
        readingId: insertId,
        error: ticketReading.error,
        readingTime: ticketReading.readingTime,
        access: ticketReading.access,
        deviceId: ticketReading.deviceId,
        errorMessage: ticketReading.errorMessage,
      ));
    }
  }

  // update ticket reading
  updateTicketReading(TicketReading newTicketReading) async {
    final db = await database;
    var res = await db!.update("TicketReadings", newTicketReading.toMap(),
        where: "id = ?", whereArgs: [newTicketReading.id]);
    return res;
  }

  // Counting total access for ticketId in device checkpointID
  Future<int?> totalAccessForTicket(int ticketId) async {
    // Fetching db
    final db = await database;

    // Total access query
    int? totalAccess = Sqflite.firstIntValue(await db!.rawQuery(
        "SELECT COUNT (*) from  TicketReadings WHERE ticketId=? AND checkpointID=? AND access=1 AND error=0",
        [ticketId, conf!.checkpointID]));

    print(totalAccess.toString() +
        " access found for ticketId=" +
        ticketId.toString() +
        " AND checkpointID=" +
        conf!.checkpointID.toString());

    return totalAccess;
  }

  // Counting total exits for ticketId in device checkpointID
  Future<int?> totalExitsForTicket(int ticketId) async {
    // Fetching db
    final db = await database;

    // Total exits query
    int? totalExits = Sqflite.firstIntValue(await db!.rawQuery(
        "SELECT COUNT (*) from  TicketReadings WHERE ticketId=? AND checkpointID=? AND access=0 AND error=0",
        [ticketId, conf!.checkpointID]));

    print(totalExits.toString() +
        " exits found for ticketId=" +
        ticketId.toString() +
        " AND checkpointID=" +
        conf!.checkpointID.toString());

    return totalExits;
  }

  // Verifying if orderId-ticketId has access with defined amount
  Future verifyAccess(TicketReading _ticketReading, Ticket _ticket) async {
    print('verifyAccess');

    int? access = await totalAccessForTicket(_ticketReading.ticketId);
    int? outs = await totalExitsForTicket(_ticketReading.ticketId);

    print("Accesos: " + access.toString());
    print("Total permitido: " + _ticket.amount.toString());

    int _remainingAccessForTicket = access! - outs!;

    if (_remainingAccessForTicket < _ticket.amount) {
      // Access authorized
      print('Access authorized');
      _ticketReading.error = false;
      _ticketReading.errorMessage = "";
    } else {
      // Access denied
      print('Access denied');
      _ticketReading.error = true;
      _ticketReading.errorMessage = "Número máximo de accesos";
    }

    // Inserting reading into db
    await insertTicketReading(_ticketReading);
  }

  // Verifying if orderId-ticketId has out permission with defined amount
  Future verifyExit(TicketReading _ticketReading, Ticket _ticket) async {
    print('verifyExit');
    int? access = await totalAccessForTicket(_ticketReading.ticketId);
    int? outs = await totalExitsForTicket(_ticketReading.ticketId);

    print("Accesos: " + access.toString());
    print("Salidas: " + outs.toString());
    print("Total permitido: " + _ticket.amount.toString());

    if (access! > outs!) {
      // Exit authorized
      print('Inserting success reading');

      // Inserting reading into db
      _ticketReading.error = false;
      _ticketReading.errorMessage = "";
    } else if (access <= outs) {
      // Exit denied
      _ticketReading.error = true;
      _ticketReading.errorMessage = "Número máximo de salidas";
    }

    // Inserting reading into db
    await insertTicketReading(_ticketReading);
  }

  // Get ticket reading by id
  Future<TicketReading?> getTicketReading(int id) async {
    final db = await database;
    var res =
        await db!.query("TicketReadings", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? TicketReading.fromMap(res.first) : null;
  }

  // get all ticket readings
  Future<List<TicketReading>> getAllTicketReadings() async {
    print("Fetching ticket readings from db");
    final db = await database;
    var res = await db!.query("TicketReadings");

    // print('Response');
    // print(res.toString());

    List<TicketReading> list =
        res.isNotEmpty ? res.map((c) => TicketReading.fromMap(c)).toList() : [];

    print(list.toString());

    return list;
  }

  // get ticket readings by checkpoint
  Future<List<TicketReading>> getAllTicketReadingsForCheckpoint() async {
    // Fetching device id
    String _deviceId = await getDeviceId();

    print("Fetching ticket readings from db for checkpoint " +
        conf!.checkpointID.toString() +
        " and deviceId = " +
        _deviceId);

    final db = await database;
    var res = await db!.query("TicketReadings",
        where: "checkpointId = ? AND deviceId=? ORDER BY readingTime DESC",
        whereArgs: [conf!.checkpointID, _deviceId]);

    List<TicketReading> list =
        res.isNotEmpty ? res.map((c) => TicketReading.fromMap(c)).toList() : [];

    print(list.toString());

    return list;
  }

  // Delete ticket reading by id
  Future deleteTicketReading(int id) async {
    final db = await database;
    return db!.delete("TicketReadings", where: "id = ?", whereArgs: [id]);
  }

  // Delete ticket reading by checkpoint
  Future deleteTicketReadingForCheckpoint(int ticketId) async {
    final db = await database;
    print("Deleting ticketId = " +
        ticketId.toString() +
        " for checkpointId = " +
        conf!.checkpointID.toString());

    return db!.delete("TicketReadings",
        where: "ticketId = ? AND checkpointId=? AND error=?",
        whereArgs: [ticketId, conf!.checkpointID, 0]);
  }

  // Delete all records
  Future deleteAll() async {
    final db = await database;
    db!.rawDelete("DELETE FROM TicketReadings");

    // Delete all ticket readings from QRReading db
    print("About to delete ticket readings from QRSERVER");
    await QRReadingsDB.db.deleteAllTicketReadings();
  }

  /*
  *
  *
  *
  * HASTA AQUI LA REVISION
  *
  *
  *
  */

  // synchronizedOrDesynchronize(TicketReading item) async {
  //   final db = await database;
  //   TicketReading blocked = TicketReading(
  //       id: item.id,
  //       orderId: item.orderId,
  //       ticketId: item.ticketId,
  //       checkpointId: item.checkpointId,
  //       ticketReadingId: item.ticketReadingId,
  //       amount: item.amount,
  //       name: item.name,
  //       error: item.error,
  //       synchronized: item.synchronized,
  //       readingTime: item.readingTime,
  //       type: item.type,
  //       exits: item.exits,
  //       deviceId: item.deviceId);
  //   var res = await db.update("TicketReading", blocked.toMap(),
  //       where: "id = ?", whereArgs: [item.id]);
  //   return res;
  // }

  // get bool ticket reading by id from db qrserver
  Future<bool> existsTicketReadingTicketReadingId(int id) async {
    final db = await database;
    var res = await db!
        .query("TicketReadings", where: "ticketReadingId = ?", whereArgs: [id]);
    return res.isNotEmpty ? true : false;
  }

  // get list of ticketReadings by type
  Future<List<TicketReading>> getTypedTicketReading(String type) async {
    final db = await database;
    var res =
        await db!.query("TicketReadings", where: "type = ?", whereArgs: [type]);
    List<TicketReading> list =
        res.isNotEmpty ? res.map((c) => TicketReading.fromMap(c)).toList() : [];
    return list;
  }

  // get tickets readings with synchronized in false
  Future<List<TicketReading>> getNotSynchronizedTicketReading() async {
    final db = await database;
    var res = await db!
        .query("TicketReadings", where: "synchronized = ?", whereArgs: [0]);
    List<TicketReading> list =
        res.isNotEmpty ? res.map((c) => TicketReading.fromMap(c)).toList() : [];
    return list;
  }

  // get tickets readings with synchronized in true
  Future<List<TicketReading>> getSynchronizedTicketReading() async {
    final db = await database;
    var res = await db!
        .query("TicketReadings", where: "synchronized = ? ", whereArgs: [1]);

    List<TicketReading> list =
        res.isNotEmpty ? res.map((c) => TicketReading.fromMap(c)).toList() : [];
    return list;
  }
}
