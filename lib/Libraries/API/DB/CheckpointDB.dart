part of db_library;

class CheckpointsDB {
  CheckpointsDB._();

  static final CheckpointsDB db = CheckpointsDB._();

  Database? _database;
  int _databaseVersion = 1;
  String _databaseName = "pumasCheckpoints.db";
  String _databaseStructure = "CREATE TABLE Checkpoints ("
      "id INTEGER PRIMARY KEY,"
      "name TEXT,"
      "parent INTEGER,"
      "idVenue INTEGER,"
      "type TEXT"
      ")";

  Future<Database?> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Init db
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
        version: _databaseVersion,
        onOpen: (db) {},
        onCreate: _onCreate,
        onUpgrade: _onUpgrade);
  }

  // _onCreate Script
  void _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  // _onUpgrade Script
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS Checkpoints");
      await db.execute(_databaseStructure);
    }
  }

  newCheckpoint(Checkpoint newCheckpoint) async {
    final db = await database;
    var raw = await db!.rawInsert(
        "INSERT Into Checkpoints (id, name, parent, idVenue, type)"
        " VALUES (?,?,?,?,?)",
        [
          newCheckpoint.id,
          newCheckpoint.name,
          newCheckpoint.parent,
          newCheckpoint.idVenue,
          newCheckpoint.type
        ]);
    return raw;
  }

  updateCheckpoint(Checkpoint newCheckpoint) async {
    final db = await database;
    var res = await db!.update("Checkpoints", newCheckpoint.toMap(),
        where: "id = ?", whereArgs: [newCheckpoint.id]);
    return res;
  }

  Future<Checkpoint?> getCheckpoint(int id) async {
    final db = await database;
    var res = await db!.query("Checkpoints", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? Checkpoint.fromMap(res.first) : null;
  }

  Future<List<Checkpoint>> getAllCheckpoints() async {
    final db = await database;
    var res = await db!.query("Checkpoints");
    List<Checkpoint> list =
        res.isNotEmpty ? res.map((c) => Checkpoint.fromMap(c)).toList() : [];
    return list;
  }

  deleteCheckpoint(int id) async {
    print("Deleting checkpoint");

    final db = await database;
    return db!.delete("Checkpoint", where: "id = ?", whereArgs: [id]);
  }

  deleteAll() async {
    print("Deleting checkpoints");

    final db = await database;
    db!.rawDelete("DELETE FROM Checkpoints");
  }

  Future<bool> getCheckpointName(String name) async {
    final db = await database;
    var res =
        await db!.query("Checkpoints", where: "name = ?", whereArgs: [name]);
    return res.isNotEmpty ? true : false;
  }

  Future<List<Checkpoint>> getVenueCheckpoint(int idVenue) async {
    final db = await database;
    var res = await db!
        .query("Checkpoints", where: "idVenue = ?", whereArgs: [idVenue]);
    List<Checkpoint> list =
        res.isNotEmpty ? res.map((c) => Checkpoint.fromMap(c)).toList() : [];
    return list;
  }

  Future<List<DDEntry>> getEntryCheckpoint(int idVenue) async {
    final db = await database;
    var res = await db!
        .query("Checkpoints", where: "idVenue = ?", whereArgs: [idVenue]);
    List<Checkpoint> list =
        res.isNotEmpty ? res.map((c) => Checkpoint.fromMap(c)).toList() : [];
    List<DDEntry> checkpoints = [];
    for (var v in list) {
      DDEntry a = DDEntry(key: v.id.toString(), value: v.name);
      checkpoints.add(a);
    }
    return checkpoints;
  }

  Future<List<Checkpoint>> getCheckpointByType(String type) async {
    final db = await database;
    var res =
        await db!.query("Checkpoints", where: "type =?", whereArgs: [type]);
    List<Checkpoint> list =
        res.isNotEmpty ? res.map((c) => Checkpoint.fromMap(c)).toList() : [];

    return list;
  }
}
