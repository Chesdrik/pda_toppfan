part of db_library;

class ErrorDB {
  ErrorDB._();

  static final ErrorDB db = ErrorDB._();

  Database? _database;
  int _databaseVersion = 1;
  String _databaseName = "pumasErrors.db";
  String _databaseStructure = "CREATE TABLE Errors ("
      "id INTEGER PRIMARY KEY AUTOINCREMENT,"
      "idType INTEGER,"
      "readingType TEXT,"
      "errorType TEXT,"
      "readingString TEXT,"
      "readingTime TEXT,"
      "deviceId TEXT,"
      "checkpointId INTEGER,"
      "sync BIT"
      ")";

  Future<Database?> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Init db
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
        version: _databaseVersion,
        onOpen: (db) {},
        onCreate: _onCreate,
        onUpgrade: _onUpgrade);
  }

  // _onCreate Script
  void _onCreate(Database db, int version) async {
    await db.execute(_databaseStructure);
  }

  // _onUpgrade Script
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    if (oldVersion < newVersion) {
      await db.rawDelete("DROP TABLE IF EXISTS Errors");
      await db.execute(_databaseStructure);
    }
  }

  Future<void> getTableStructure() async {
    final db = await database;
    var returneddata = await db!.rawQuery("PRAGMA table_info(Errors)");

    print('getTableStructure');
    print(returneddata);
  }

  insertErrors(ErrorModel error) async {
    final db = await database;
    print('Inserting error');

    // Inserting error in table
    var insertQuery = await db!.rawInsert(
        "INSERT INTO Errors (idType, readingType, errorType, readingString, readingTime, deviceId, checkpointId, sync)"
        " VALUES (?,?,?,?,?,?,?,?)",
        [
          error.idType,
          error.readingType,
          error.errorType,
          error.readingString,
          error.readingTime,
          error.deviceId,
          error.checkpointId,
          error.sync
        ]);
    print('error insert');
    return insertQuery;
  }

  // Update errors
  updateErrors(ErrorModel updateError) async {
    final db = await database;
    var res = await db!.update("Errors", updateError.toMap(),
        where: "id = ?", whereArgs: [updateError.id]);
    return res;
  }

  // Get error by id
  Future<ErrorModel?> getError(int id) async {
    final db = await database;
    var res = await db!.query("Errors", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? ErrorModel.fromMap(res.first) : null;
  }

  // Get all errors
  Future<List<ErrorModel>> getAllErrors() async {
    print("Fetching accreditation readings from db");
    final db = await database;
    var res = await db!.query("Errors");

    print('Response');
    print(res.toString());

    List<ErrorModel> list =
        res.isNotEmpty ? res.map((c) => ErrorModel.fromMap(c)).toList() : [];

    print(list.toString());

    return list;
  }

  // Delete ticket reading by id
  Future deleteError(int id) async {
    final db = await database;
    db!.delete("Errors", where: "id = ?", whereArgs: [id]);
  }

  // Delete all records
  Future deleteAll() async {
    final db = await database;
    db!.rawDelete("DELETE FROM Errors");
  }

  // Get accreditation readings with synchronized in false
  Future<List<ErrorModel>> getNotSynchronizedErrors() async {
    final db = await database;
    var res = await db!.query("Errors", where: "sync = ?", whereArgs: [0]);
    List<ErrorModel> list =
        res.isNotEmpty ? res.map((c) => ErrorModel.fromMap(c)).toList() : [];
    return list;
  }
}
