library api_library;

// Required imports
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:pda_toppfan/Libraries/API/DB/DBLibrary.dart';
import 'dart:async';
import 'dart:convert';
import 'package:pda_toppfan/Libraries/GlobalData/GlobalLibrary.dart' as globals;
import 'package:pda_toppfan/Libraries/Helpers/HelpersLibrary.dart';
import 'package:pda_toppfan/Models/Access.dart';
import 'package:pda_toppfan/Models/Accreditation.dart';
import 'package:pda_toppfan/Models/AccreditationReading.dart';
import 'package:pda_toppfan/Models/Alert.dart';
import 'package:pda_toppfan/Models/Checkpoint.dart';
import 'package:pda_toppfan/Models/Configuration.dart';
import 'package:pda_toppfan/Models/ErrorModel.dart';
import 'package:pda_toppfan/Models/RequirementAccess.dart';
import 'package:pda_toppfan/Models/ResponseModel.dart';
import 'package:pda_toppfan/Models/DDEntry.dart';
import 'package:pda_toppfan/Models/Event.dart';
import 'package:pda_toppfan/Models/Ticket.dart';
import 'package:pda_toppfan/Models/TicketReading.dart';
import 'package:pda_toppfan/Models/TicketType.dart';
import 'package:pda_toppfan/Models/Venue.dart';
import 'package:pda_toppfan/Scanner/ScannerFunctions.dart';
import 'package:pda_toppfan/Scanner/ScannerWidgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

//parts
part 'package:pda_toppfan/Libraries/API/Functions/APIFunctions.dart';
part 'package:pda_toppfan/Libraries/API/Functions/APIInformation.dart';
part 'package:pda_toppfan/Libraries/API/Functions/APIGetDataToPDA.dart';
part 'package:pda_toppfan/Libraries/API/Functions/APITickets.dart';
part 'package:pda_toppfan/Libraries/API/Functions/APIAccreditation.dart';
