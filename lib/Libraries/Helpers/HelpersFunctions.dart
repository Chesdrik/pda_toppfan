part of helpers;

/// {@category Basics}
/// {@category Color, HexColor}
/// {@subCategory Information displays}
class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

/// {@category Basics}
/// {@category Information, Device Information}
/// {@subCategory Information displays}
Future<String> getDeviceId() async {
  String identifier = '';
  final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
  try {
    if (Platform.isAndroid) {
      var build = await deviceInfoPlugin.androidInfo;
      identifier = build.androidId; //UUID for Android
    } else if (Platform.isIOS) {
      var data = await deviceInfoPlugin.iosInfo;
      identifier = data.identifierForVendor; //UUID for iOS
    }
  } on PlatformException {
    print('Failed to get platform version');
  }

  return identifier;
}

/// {@category Basics}
/// {@category Information, Device Information, battery information}
/// {@subCategory Information displays}
class Battery {
  /// Returns the current battery level in percent.
  Future<int> get batteryLevel async =>
      await BatteryPlatform.instance.batteryLevel();

  /// Fires whenever the battery state changes.
  Stream<BatteryState> get onBatteryStateChanged =>
      BatteryPlatform.instance.onBatteryStateChanged();
}
