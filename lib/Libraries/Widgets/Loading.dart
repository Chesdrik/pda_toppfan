part of widget_library;

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: lightGray,
      child: SpinKitThreeBounce(
        color: primaryColor,
        size: 30.0,
      ),
    );
  }
}

class ContaineddLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SpinKitThreeBounce(
        color: primaryColor,
        size: 30.0,
      ),
    );
  }
}

class RippleLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SpinKitRipple(
        color: primaryColor,
        size: 100.0,
      ),
    );
  }
}
