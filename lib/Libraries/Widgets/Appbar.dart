part of widget_library;

// ignore: must_be_immutable
class AppbarPDA extends StatefulWidget {
  AppbarPDA({
    Key? key,
    required this.popType,
    required this.appBarPDATitle,
  }) : super(key: key);
  var popType;
  String appBarPDATitle;
  @override
  _AppbarPDAState createState() => _AppbarPDAState();
}

class _AppbarPDAState extends State<AppbarPDA> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      brightness: Brightness.dark,
      backgroundColor: primaryColor,
      elevation: 0,
      leading: IconButton(
        icon: new Icon(Icons.arrow_back_ios),
        color: lightGray,
        onPressed: () {
          if (widget.popType == 'pop') {
            Navigator.pop(context);
          } else {
            Navigator.popUntil(context, ModalRoute.withName(widget.popType));
          }
        },
      ),
      centerTitle: true,
      title: Text(
        widget.appBarPDATitle,
        style: TextStyle(
          color: lightGray,
        ),
      ),
    );
  }
}
