part of widget_library;

// ignore: must_be_immutable
class Dropdown extends StatefulWidget {
  String label;
  String value;
  List<DDEntry> values;
  Function(String) callback;

  Dropdown(this.label, this.value, this.values, this.callback);

  @override
  _DropdownState createState() => _DropdownState();
}

class _DropdownState extends State<Dropdown> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        children: <Widget>[
          Container(
              child: Text(widget.label,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  )),
              width: double.infinity,
              padding: EdgeInsets.only(bottom: 10)),
          DropdownButton<String>(
            isExpanded: true,
            value: widget.value.toString(),
            icon: Icon(Icons.arrow_drop_down),
            iconSize: 24,
            elevation: 16,
            style: TextStyle(color: Colors.black38),
            underline: Container(
              height: 2,
              color: Colors.black38,
            ),
            onChanged: (String? newValue) {
              setState(() {
                widget.callback(newValue!);
                widget.value = newValue;
              });
            },
            hint: Text('Selecciona una opción'),
            items: widget.values
                .map<DropdownMenuItem<String>>(
                    (value) => new DropdownMenuItem<String>(
                          value: value.key,
                          child: new Text(value.value),
                        ))
                .toList(),
          ),
        ],
      ),
    );
  }
}
