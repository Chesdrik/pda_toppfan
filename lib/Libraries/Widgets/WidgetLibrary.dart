library widget_library;

import 'package:flutter/material.dart';
import 'package:pda_toppfan/Libraries/GlobalData/GlobalLibrary.dart';
import 'package:pda_toppfan/Models/DDEntry.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

//part
part 'package:pda_toppfan/Libraries/Widgets/HomeButton.dart';
part 'package:pda_toppfan/Libraries/Widgets/Appbar.dart';
part 'package:pda_toppfan/Libraries/Widgets/Loading.dart';
part 'package:pda_toppfan/Libraries/Widgets/Dropdown.dart';
part 'package:pda_toppfan/Libraries/Widgets/Dialogs.dart';
