part of widget_library;

// ignore: must_be_immutable
class HomeButton extends StatefulWidget {
  late String title;
  late String action;
  late IconData icon;
  String type = 'text';
  double iconSize = 30;

  // Regular home button
  HomeButton({
    Key? key,
    required this.title,
    required this.action,
  }) : super(key: key) {
    this.type = 'text';
  }

  HomeButton.icon({
    Key? key,
    required this.type,
    required this.icon,
    required this.action,
    required this.iconSize,
  }) : super(key: key) {
    this.title = '';
  }

  @override
  _HomeButtonState createState() => _HomeButtonState();
}

class _HomeButtonState extends State<HomeButton> {
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        width: double.infinity,
        height: 60,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(primaryColor),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            )),
          ),
          child: (() {
            if (this.widget.type == 'text') {
              return Text(
                this.widget.title,
                style: TextStyle(
                  color: lightGray,
                  fontSize: 20,
                  letterSpacing: 3,
                ),
              );
            } else if (widget.type == 'icon') {
              return Icon(
                this.widget.icon,
                color: lightGray,
                size: widget.iconSize,
              );
            } else {
              return Text('yes');
            }
          }()),
          onPressed: () => _handleSubmit(context),
        ),
      ),
    );
  }

  Future<void> _handleSubmit(BuildContext context) async {
    print('click on ' + this.widget.action);

    try {
      if (this.widget.action == 'pop') {
        // Navigator.pop(context);
        Navigator.pushNamed(context, '/');
      } else if (this.widget.action == 'download') {
        String _url = "https://pumasgol.lampserv.com/storage/app.apk";
        print("launching url: " + _url);
        await launch(_url);
      } else {
        print("Action: " + this.widget.action);
        Navigator.pushNamed(context, this.widget.action);
      }
    } catch (error) {
      print(error);
    }
  }
}
