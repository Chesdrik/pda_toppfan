import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pda_toppfan/Admin/AdminPage.dart';
import 'package:pda_toppfan/Admin/Configuration/ConfigurationViews/ConfigurationFetch.dart';
import 'package:pda_toppfan/Admin/Configuration/SelectActionView.dart';
import 'package:pda_toppfan/Admin/Data/Accreditations/AccreditationsReadingsView.dart';
import 'package:pda_toppfan/Admin/Data/Accreditations/AccreditationsView.dart';
import 'package:pda_toppfan/Admin/Data/AllDataView.dart';
import 'package:pda_toppfan/Admin/Data/CheckpointsView.dart';
import 'package:pda_toppfan/Admin/Data/TicketTypesView.dart';
import 'package:pda_toppfan/Admin/Data/Tickets/TicketsReadingView.dart';
import 'package:pda_toppfan/Admin/Data/Tickets/TicketsView.dart';
import 'package:pda_toppfan/Admin/Data/VenuesView.dart';
import 'package:pda_toppfan/Home/HomePage.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => HomePage(),

        ///Admin routes
        '/admin': (context) => AdminPage(),
        '/configuration/type': (context) => SelectActionView(),
        '/configuration': (context) => ConfigurationFetch(),

        /// All data routes
        '/all_data': (context) => AllDataView(),
        '/tickets': (context) => TicketsView(),
        '/tickets_reading': (context) => TicketsReadingView(),
        '/accreditations/registered': (context) => AccreditationsView(),
        '/accreditations/readings': (context) => AccreditationsReadingsView(),
        '/checkpoints': (context) => CheckpointsView(),
        '/venues': (context) => VenuesView(),
        '/ticket_types': (context) => TicketTypesView(),
      },
    );
  }
}
