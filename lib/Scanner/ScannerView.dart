import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pda_toppfan/Libraries/API/DB/DBLibrary.dart';
import 'package:pda_toppfan/Libraries/API/apiLibrary.dart';
import 'package:pda_toppfan/Libraries/Helpers/HelpersLibrary.dart';
import 'package:pda_toppfan/Libraries/Widgets/WidgetLibrary.dart';
import 'package:pda_toppfan/Models/QRReading.dart';
import 'package:pda_toppfan/Models/ResponseModel.dart';
import 'package:pda_toppfan/Scanner/ScannerFunctions.dart';
import 'package:pda_toppfan/Scanner/ScannerWidgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pda_toppfan/Libraries/GlobalData/GlobalLibrary.dart';

class TicketScanner extends StatefulWidget {
  @override
  _TicketScannerState createState() => _TicketScannerState();
}

class _TicketScannerState extends State<TicketScanner>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  bool _loaded = false;
  bool _readingAccess = true;
  List<QRReading> _qrReadings = [];
  late String deviceId;
  late Timer timer;
  bool activeSync = true;
  List<bool> isSelected = [true, false];
  bool accesos = true;
  static final channelName = 'mx.pumas/qrchannel';

  @override
  void initState() {
    controller =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    controller.addListener(() {
      setState(() {});
    });
    firstRun();

    super.initState();
  }

  Future<void> firstRun() async {
    var prefs = await SharedPreferences.getInstance();
    bool offline = prefs.getBool('offline') ?? true;

    if (!offline) {
      this.timer = Timer.periodic(Duration(seconds: 15), (Timer t) async {
        print('sync');
        await ticketsSynchronization();
        await accreditationSynchronization();
        if (this.mounted) {
          setState(() {});
        }
      });
      //send error every 60 sec and check battery
      this.timer = Timer.periodic(Duration(seconds: 60), (Timer t) async {
        print('sync errors');
        await syncAlerts();
        await alertBattery();
        await syncErrors();
      });
    }
    fetchQRReadings();
  }

  Future<void> fetchQRReadings() async {
    // Setting state
    setState(() {
      _loaded = false;
    });

    // Fetching all accreditations from DB
    this._qrReadings = await QRReadingsDB.db.getAllQRReadings();
    print(this._qrReadings.toString());
    //device id
    String aux = await getDeviceId();
    // Setting state
    setState(() {
      this.deviceId = aux;
      _loaded = true;
    });
  }

  @override
  void dispose() {
    print('dispose');

    this.timer.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final methodChannel = MethodChannel(channelName);
    methodChannel.setMethodCallHandler(qrScanMethod);
    methodChannel.invokeListMethod("checkTicketScanner");

    return WillPopScope(
        onWillPop: onBackPressed(context, timer),
        child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(60.0), // here the desired height
            child: AppBar(
              title: Text(
                conf!.checkpointName,
                style: TextStyle(color: primaryColor),
              ),
              leading: new IconButton(
                  icon: new Icon(Icons.arrow_back, color: darkGray),
                  onPressed: () async {
                    ResponseModel response = await sendInfoDevice("end");
                    if (response.error) {
                      Fluttertoast.showToast(
                          msg: response.message,
                          backgroundColor:
                              response.error ? Colors.red : Colors.green);
                    } else {
                      var prefs = await SharedPreferences.getInstance();
                      bool offline = prefs.getBool('offline') ?? true;

                      if (!offline) {
                        this.timer.cancel();
                      }

                      Navigator.popUntil(context, ModalRoute.withName('/'));
                    }
                  }),
              actions: <Widget>[
                sendAlertWidget(context, controller, darkGray),
                SizedBox(
                  width: 20,
                )
              ],
              backgroundColor: Colors.transparent,
              bottomOpacity: 0.0,
              elevation: 0.0,
            ),
          ),
          body: (!_loaded
              ? RippleLoading()
              : Column(
                  // mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    getOptionsReadingWidget(
                        context, isSelected, _readingAccess),
                    SizedBox(
                      height: 10,
                    ),
                    Text(deviceId),
                    Expanded(
                      flex: 10,
                      // ignore: unnecessary_null_comparison
                      child: (this._qrReadings != null &&
                              this._qrReadings.length > 0
                          ? Column(
                              children: [
                                latestScanedElement(
                                    context, this._qrReadings[0]),
                                Expanded(
                                  child: ListView.builder(
                                      itemCount: this._qrReadings.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return (index == 0
                                            ? SizedBox(
                                                height: 0,
                                              )
                                            : scannedElement(context,
                                                this._qrReadings[index]));
                                      }),
                                ),
                              ],
                            )
                          : Column(
                              children: [
                                Spacer(),
                                RippleLoading(),
                                Text(
                                  "No hay lecturas registradas",
                                  style: TextStyle(fontSize: 20.0),
                                ),
                                Spacer(),
                              ],
                            )),
                    ),
                  ],
                )),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
          floatingActionButton: Row(
            children: [
              Spacer(),
              (scannerPDA
                  ? SizedBox.shrink()
                  : FloatingActionButton(
                      backgroundColor: primaryColor,
                      elevation: 10,
                      onPressed: () async {
                        if (reading) {
                          reading = false;
                          await cameraScan(context, this._readingAccess);
                          await fetchQRReadings();
                          setState(() {});
                        }
                      },
                      child: Icon(
                        Icons.camera,
                        color: lightGray,
                        size: 30,
                      ),
                      heroTag: "access_scan",
                    )),
              Spacer(),
            ],
          ),
        ));
  }

  Future<void> qrScanMethod(MethodCall call) async {
    switch (call.method) {
      case "didReceiveQR":
        {
          await processScannedQR(context, call.arguments, this._readingAccess);

          await fetchQRReadings();
          setState(() {});
          print('Setting state');
        }
        break;
      case "scanDevicePresent":
        {
          setState(() {
            scannerPDA = true;
          });
        }
        break;
    }
  }

  Widget getOptionsReadingWidget(context, isSelected, _readingAccess) {
    return Expanded(
      flex: 1,
      child: ToggleButtons(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextButton.icon(
                onPressed: null,
                icon: Icon(Icons.plus_one),
                label: Text("Entrada")),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextButton.icon(
                onPressed: null,
                icon: Icon(Icons.exposure_neg_1),
                label: Text("Salida")),
          ),
        ],
        onPressed: (int index) {
          setState(() {
            isSelected[0] = false;
            isSelected[1] = false;
            isSelected[index] = !isSelected[index];

            this._readingAccess = (index == 0 ? true : false);
            print(this._readingAccess);
          });
        },
        isSelected: isSelected,
      ),
    );
  }
}
