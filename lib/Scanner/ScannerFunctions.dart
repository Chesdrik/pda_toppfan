import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pda_toppfan/Libraries/API/DB/DBLibrary.dart';
import 'package:pda_toppfan/Libraries/API/apiLibrary.dart';
import 'package:pda_toppfan/Libraries/Helpers/HelpersLibrary.dart';
import 'package:pda_toppfan/Models/ErrorModel.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:encrypt/encrypt.dart' as Encrypter;

import 'package:pda_toppfan/Libraries/GlobalData/GlobalLibrary.dart';
import 'package:pda_toppfan/Scanner/ScannerWidgets.dart';

/// Method to call process qr reading
///
/// {@category camera Scan}
/// {@subCategory proccess scanner qr}
Future<void> cameraScan(BuildContext context, bool _readingAccess) async {
  String? qrRead = await scanner.scan();
  print('Tipo de salida');
  print(_readingAccess);
  await processScannedQR(context, qrRead!, _readingAccess);
}

/// Method to process qr reading
///
/// {@category camera Scan}
/// {@subCategory proccess scanner qr}
Future<void> processScannedQR(
    BuildContext context, String encodedQRString, bool _readingAccess) async {
  // Displaying alert
  showReadingAlert(context);
  var now = new DateTime.now();
  String date = DateFormat('yyyy-MM-dd HH:mm:ss').format(now).toString();
  // print('processScannedQR: ' + encodedQRString);

  Map<String, dynamic>? decodedQR = await decryptQR(encodedQRString);

  // Dividing accreditation and ticket processing
  // ignore: unnecessary_null_comparison
  if (decodedQR == null) {
    // Throwing alert in case QR is not valid
    Navigator.pop(context);
    catchError(
        null, null, 'qr', decodedQR, date, 'processScannedQR+ decodedQR null');
    reading = true;
    showAlert(context, true, 'Error', "El QR no es válido");
  } else if (decodedQR.containsKey('aID')) {
    // Processing accreditation QR
    await processScannedAccreditationQR(context, decodedQR, _readingAccess);
  } else if (decodedQR.containsKey('id')) {
    // Processing ticket QR
    await processScannedTicketQR(context, decodedQR, _readingAccess);
  } else {
    // Throwing alert in case QR is not valid
    Navigator.pop(context);
    catchError(null, null, 'qr', decodedQR, date,
        'processScannedQR + not contains key');
    reading = true;
    showAlert(context, true, 'Error', "El QR no es válido");
  }
}

/// Method to decrypt qr reading
///
/// {@category camera Scan}
/// {@subCategory proccess scanner qr}
Future<Map<String, dynamic>?> decryptQR(String stringRead) async {
  var now = new DateTime.now();
  String date = DateFormat('yyyy-MM-dd HH:mm:ss').format(now).toString();
  // Decrypting
  final key =
      Encrypter.Key.fromBase64("+T66Y/ylDOxRU1ASXrOS2EyBYV7qkxGQsxR35LWeeog=");
  final iv = Encrypter.IV.fromBase64("8PUYalVMqdZqT9iG1JAbWA==");

  // Defining encrypter and decrypting string
  final encrypter =
      Encrypter.Encrypter(Encrypter.AES(key, mode: Encrypter.AESMode.cbc));

  try {
    final decrypted = encrypter.decrypt64(stringRead, iv: iv);

    // Reading QR and decoding json
    return json.decode(decrypted);
  } on FormatException catch (e) {
    print('FormatException cachada');
    catchError(null, null, 'qr', stringRead, date, 'decryptQR' + e.toString());
    return null;
  } catch (e) {
    print('Error modafaka!');
    print(e);
    catchError(null, null, 'qr', stringRead, date, 'decryptQR' + e.toString());
    return null;
  }
}

/// Method to insert error into db error if not exist
/// connection internet
/// {@category error Informations}
/// {@subCategory Information display}
void catchError(
    idType, readingType, errorType, readingString, readingTime, from) async {
  print("Catch error");
  print(from);

  ErrorModel error = new ErrorModel(
      idType: idType,
      readingType: readingType,
      errorType: errorType,
      readingString: readingString.toString(),
      readingTime: readingTime,
      deviceId: await getDeviceId(),
      checkpointId: conf!.checkpointID,
      sync: false,
      id: 0);

  await ErrorDB.db.insertErrors(error);
}
