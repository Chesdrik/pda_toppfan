import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pda_toppfan/Libraries/API/apiLibrary.dart';
import 'package:pda_toppfan/Libraries/GlobalData/GlobalLibrary.dart';
import 'package:pda_toppfan/Libraries/Helpers/HelpersLibrary.dart';
import 'package:pda_toppfan/Models/QRReading.dart';
import 'package:pda_toppfan/Models/ResponseModel.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

onBackPressed(context, timer) {
  return Alert(
    context: context,
    title: "Alerta",
    type: AlertType.info,
    desc: "¿Estás seguro de querer salirte del lector?",
    buttons: [
      DialogButton(
        onPressed: () => Navigator.pop(context),
        child: Text(
          "No",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
      ),
      DialogButton(
        onPressed: () async {
          timer?.cancel();
          ResponseModel response = await sendInfoDevice("end");
          if (response.error) {
            Fluttertoast.showToast(
                msg: response.message,
                backgroundColor: response.error ? Colors.red : Colors.green);
          }
          Navigator.popUntil(context, ModalRoute.withName('/'));
        },
        child: Text(
          "Si",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
      )
    ],
  ).show();
}

sendAlertWidget(context, controller, color) {
  return GestureDetector(
    child: Stack(
      alignment: Alignment.center,
      children: <Widget>[
        SizedBox(
          child: CircularProgressIndicator(
            value: 1.0,
            valueColor: AlwaysStoppedAnimation<Color>(Colors.grey),
          ),
          height: 50,
          width: 50,
        ),
        SizedBox(
          child: CircularProgressIndicator(
            value: controller.value,
            valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
          ),
          height: 50,
          width: 50,
        ),
        Icon(
          Icons.add_alert,
          color: color,
          size: 40,
        )
      ],
    ),
    onTapDown: (_) => controller.forward(),
    onTapUp: (_) async {
      if (controller.status == AnimationStatus.forward) {
        controller.reverse();
      }
      ResponseModel response = await sendAlert();
      controller.reset();

      Fluttertoast.showToast(
          toastLength: Toast.LENGTH_LONG,
          msg: response.message,
          backgroundColor: response.error ? Colors.red : Colors.green);
    },
  );
}

Widget latestScanedElement(BuildContext context, QRReading qrReading) {
  // Error color
  String _color = "#FF5252";
  String _text = qrReading.errorMessage;
  // String _text = "";
  IconData _icon = Icons.report;

  if (!qrReading.error) {
    if (qrReading.access) {
      // Exit with success
      _color = "#8BC34A";
      _text = "Acceso autorizado";
      _icon = Icons.check;
    } else {
      // Access with success
      _color = "#03A9F4";
      _text = "Salida autorizada";
      _icon = Icons.restore_outlined;
    }
  }

  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(
      height: MediaQuery.of(context).size.height / 4,
      // height: 150.0,
      width: double.infinity,
      child: Card(
        elevation: 2,
        color: HexColor(_color).withOpacity(0.75),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
              child: Column(
            children: [
              Spacer(flex: 2),
              Icon(
                _icon,
                color: Colors.white,
                size: 50.0,
              ),
              Spacer(),
              Text(
                _text,
                style: TextStyle(fontSize: 25.0, color: Colors.white),
                textAlign: TextAlign.center,
              ),
              Text(
                qrReading.id.toString(),
                style: TextStyle(fontSize: 15.0, color: Colors.white),
                textAlign: TextAlign.center,
              ),
              Spacer(flex: 2),
            ],
          )),
        ),
      ),
    ),
  );
  // return Text('SCAN!');
}

Widget scannedElement(BuildContext context, QRReading qrReading) {
  // Error color
  String _color = "#FF5252";
  String _text = qrReading.errorMessage;

  if (!qrReading.error) {
    if (qrReading.access) {
      // Access with success
      _color = "#8BC34A";
      _text = "Acceso autorizado";
    } else {
      // Exit with success
      _color = "#03A9F4";
      _text = "Salida autorizada";
    }
  }

  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(
      width: double.infinity,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 1.0,
        child: Container(
          height: 70,
          child: ListTile(
            leading: Container(
                width: 40,
                child: CircleAvatar(
                  backgroundColor: HexColor(_color).withOpacity(0.75),
                  foregroundColor: HexColor("#FFFFFF"),
                  radius: 20,
                  child: Text(qrReading.id.toString()),
                )),
            title: Text(_text),
            subtitle: Text(parseDateShort(qrReading.readingTime)),
          ),
        ),
      ),
    ),
  );
}

void showReadingAlert(BuildContext context) {
  Alert(
    context: context,
    type: AlertType.info,
    title: "Leyendo Acceso",
    content: Column(
      children: <Widget>[
        SpinKitThreeBounce(
          color: primaryColor,
          size: 50.0,
        ),
      ],
    ),
  ).show();
}

void showAlert(BuildContext context, bool error, String title, String message) {
  Alert(
    context: context,
    type: (error ? AlertType.error : AlertType.info),
    // type: AlertType.info,
    title: title,
    desc: message,
    style: AlertStyle(
      animationType: AnimationType.grow,
      animationDuration: Duration(milliseconds: 1),
    ),
    buttons: [
      DialogButton(
        child: Text(
          "Ok",
          style: TextStyle(color: Colors.white, fontSize: 12),
        ),
        onPressed: () {
          Navigator.pop(context);
        },
        width: 120,
      )
    ],
  ).show();
}
