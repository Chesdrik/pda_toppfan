import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pda_toppfan/Libraries/API/apiLibrary.dart';
import 'package:pda_toppfan/Libraries/GlobalData/GlobalLibrary.dart'
    as colorsGlobals;
import 'package:pda_toppfan/Libraries/Helpers/HelpersLibrary.dart';
import 'package:pda_toppfan/Models/Configuration.dart';
import 'package:pda_toppfan/Models/ResponseModel.dart';

// ignore: must_be_immutable
class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);
  int _count = 0;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late List<Configuration> list;
  late Configuration conf;
  bool existConf = false;
  String deviceId = "";

  @override
  void initState() {
    _conf();
    super.initState();
  }

  _conf() async {
    String aux = await getDeviceId();
    // list = await ConfigurationsDBProvider.db.getAllConfigurations();
    setState(() {
      this.deviceId = aux;
    });
    // if (list.isNotEmpty) {
    //   Configuration aux = await ConfigurationsDBProvider.db.getConfig();
    //   setState(() {
    //     this.existConf = true;
    //     this.conf = aux;
    //     globals.conf = this.conf;
    //   });
    // }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Container(
        width: double.infinity,
        child: Column(
          children: [
            Spacer(flex: 5),
            GestureDetector(
              onTap: () {
                setState(() {
                  widget._count++;
                });
              },
              onLongPress: () {
                print(widget._count.toString());
                if (widget._count >= 2) {
                  setState(() {
                    widget._count = 0;
                  });
                  Navigator.pushNamed(context, '/admin');
                }
              },
              child: Image.asset('assets/logo_pumas.png', width: 150),
            ),
            Spacer(),
            Text(
              this.deviceId,
              style: TextStyle(fontSize: 20),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: double.infinity,
                child: TextButton(
                  onPressed: () async {
                    ResponseModel response = await sendInfoDevice("start");
                    if (response.error) {
                      Fluttertoast.showToast(
                          msg: response.message,
                          backgroundColor:
                              response.error ? Colors.red : Colors.green);
                    } else {
                      Navigator.popUntil(context, ModalRoute.withName('/'));
                    }
                    Navigator.pushNamed(context, '/tickets/scan');
                  },
                  style: TextButton.styleFrom(
                      primary: colorsGlobals.buttonTextColor,
                      backgroundColor: colorsGlobals.buttonBackgroundColor,
                      textStyle: TextStyle(fontSize: 18.0)),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('QRs'),
                  ),
                ),
              ),
            ),
            Spacer(flex: 5),
          ],
        ),
      )),
    );
  }
}
