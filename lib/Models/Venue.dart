import 'dart:convert';

Venue itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Venue.fromMap(jsonData);
}

String itemToJson(Venue data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Venue {
  int id;
  String name;

  Venue({required this.id, required this.name});

  factory Venue.fromMap(Map<String, dynamic> json) => new Venue(
        id: json["id"],
        name: json["name"],
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
      };

  Map<String, dynamic> toJson() => {
        'id': this.id,
        'name': this.name,
      };
}
