import 'dart:convert';

Alert itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Alert.fromMap(jsonData);
}

String itemToJson(Alert data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Alert {
  int id;
  String message;
  String type;
  bool sync;
  bool error;
  int checkpointId;
  int eventId;
  String deviceId;

  Alert({
    required this.id,
    required this.message,
    required this.type,
    required this.sync,
    required this.error,
    required this.checkpointId,
    required this.eventId,
    required this.deviceId,
  });

  factory Alert.fromMap(Map<String, dynamic> json) => new Alert(
        id: json["id"],
        message: json["message"],
        type: json["type"],
        sync: (json["sync"] == 0 ? false : true),
        error: (json["sync"] == 0 ? false : true),
        checkpointId: json["checkpointId"],
        eventId: json["eventId"],
        deviceId: json["deviceId"],
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'message': message,
        'type': type,
        'sync': sync,
        'error': error,
        "checkpointId": checkpointId,
        "eventId": eventId,
        "deviceId": deviceId,
      };

  Map<String, dynamic> toJson() => {
        'id': this.id,
        'message': this.message,
        'type': this.type,
        'sync': this.sync,
        'error': this.error,
        "checkpointId": this.checkpointId,
        "eventId": this.eventId,
        "deviceId": this.deviceId,
      };
}
