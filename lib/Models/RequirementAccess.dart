import 'dart:convert';

RequirementAccess itemFromJson(String str) {
  final jsonData = json.decode(str);
  return RequirementAccess.fromMap(jsonData);
}

String itemToJson(RequirementAccess data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class RequirementAccess {
  int id;
  int idAccess;
  int accessRequirementId;
  String type;
  String request;
  String dataType;
  String status;

  RequirementAccess(
      {required this.id,
      required this.idAccess,
      required this.accessRequirementId,
      required this.type,
      required this.request,
      required this.dataType,
      required this.status});

  factory RequirementAccess.fromMap(Map<String, dynamic> json) =>
      new RequirementAccess(
          id: json['id'],
          idAccess: json["idAccess"],
          accessRequirementId: json["accessRequirementId"],
          type: json["type"],
          request: json["request"],
          dataType: json["dataType"],
          status: json["status"]);

  Map<String, dynamic> toMap() => {
        'id': id,
        'idAccess': idAccess,
        'accessRequirementId': accessRequirementId,
        'type': type,
        'request': request,
        'dataType': dataType,
        'status': status,
      };

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'idAccess': idAccess,
      'accessRequirementId': accessRequirementId,
      'type': type,
      'request': request,
      'dataType': dataType,
      'status': status,
    };
  }
}
