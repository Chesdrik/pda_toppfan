class Event {
  int id;
  String name;
  String date;

  Event(this.id, this.name, this.date);

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'date': date,
      };
}
