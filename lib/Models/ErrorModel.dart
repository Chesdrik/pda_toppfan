import 'dart:convert';

ErrorModel itemFromJson(String str) {
  final jsonData = json.decode(str);
  return ErrorModel.fromMap(jsonData);
}

String itemToJson(ErrorModel data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class ErrorModel {
  int id;
  int idType;
  String readingType;
  String errorType;
  String readingString;
  String readingTime;
  String deviceId;
  int checkpointId;
  bool sync;

  ErrorModel(
      {required this.id,
      required this.idType,
      required this.readingType,
      required this.errorType,
      required this.readingString,
      required this.readingTime,
      required this.deviceId,
      required this.checkpointId,
      required this.sync});

  factory ErrorModel.fromMap(Map<String, dynamic> json) => new ErrorModel(
      id: json["id"],
      idType: json["idType"],
      readingType: json["readingType"],
      errorType: json["errorType"],
      readingString: json["readingString"],
      readingTime: json["readingTime"],
      deviceId: json["deviceId"],
      checkpointId: json["checkpointId"],
      sync: (json["sync"] == 0 ? false : true));

  Map<String, dynamic> toMap() => {
        'id': id,
        'idType': idType,
        'readingType': readingType,
        'errorType': errorType,
        'readingString': readingString,
        'readingTime': readingTime,
        'deviceId': deviceId,
        'checkpointId': checkpointId,
        'sync': sync
      };
  Map<String, dynamic> toJson() => {
        'id': this.id,
        'idType': this.idType,
        'readingType': this.readingType,
        'errorType': this.errorType,
        'readingString': this.readingString,
        'readingTime': this.readingTime,
        'deviceId': this.deviceId,
        'checkpointId': this.checkpointId,
        'sync': this.sync
      };
}
