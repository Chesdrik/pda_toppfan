import 'dart:convert';

Accreditation itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Accreditation.fromMap(jsonData);
}

String itemToJson(Accreditation data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Accreditation {
  int id;
  int accreditationId;
  int eventId;
  int ticketTypeId;
  int version;
  String name;
  List<int> checkpoints;

  Accreditation({
    required this.id,
    required this.accreditationId,
    required this.eventId,
    required this.ticketTypeId,
    required this.version,
    required this.name,
    required this.checkpoints,
  });

  factory Accreditation.fromMap(Map<String, dynamic> json) => new Accreditation(
        id: json["id"],
        accreditationId: json["accreditationId"],
        eventId: json["eventId"],
        ticketTypeId: json["ticketTypeId"],
        version: json["version"],
        name: json["name"],
        checkpoints: jsonDecode(json["checkpoints"]).cast<int>(),
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'accreditationId': accreditationId,
        'eventId': eventId,
        'ticketTypeId': ticketTypeId,
        'version': version,
        'name': name,
        'checkpoints': checkpoints,
      };
}
