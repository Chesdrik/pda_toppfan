import 'dart:convert';

ResponseModel itemFromJson(String str) {
  final jsonData = json.decode(str);
  return ResponseModel.fromMap(jsonData);
}

String itemToJson(ResponseModel data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class ResponseModel {
  bool error;
  String message;

  ResponseModel({
    required this.error,
    required this.message,
  });

  factory ResponseModel.fromMap(Map<String, dynamic> json) => new ResponseModel(
        error: json["error"],
        message: json["message"],
      );

  Map<String, dynamic> toMap() => {
        'error': error,
        'message': message,
      };
}
