import 'dart:convert';

TicketType itemFromJson(String str) {
  final jsonData = json.decode(str);
  return TicketType.fromMap(jsonData);
}

String itemToJson(TicketType data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class TicketType {
  int id;
  String name;
  String price;
  String color;
  int venueId;

  TicketType({
    required this.id,
    required this.name,
    required this.price,
    required this.color,
    required this.venueId,
  });

  factory TicketType.fromMap(Map<String, dynamic> json) => new TicketType(
        id: json["id"],
        name: json["name"],
        price: json["price"],
        color: json["color"],
        venueId: json["venueId"],
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'price': price,
        'color': color,
        'venueId': venueId,
      };
}
