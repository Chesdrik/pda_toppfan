import 'dart:convert';
import 'package:pda_toppfan/Libraries/API/DB/DBLibrary.dart';
import 'package:pda_toppfan/Models/Ticket.dart';

TicketReading itemFromJson(String str) {
  final jsonData = json.decode(str);
  return TicketReading.fromMap(jsonData);
}

String itemToJson(TicketReading data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class TicketReading {
  int id;
  int orderId;
  int ticketId;
  int checkpointId;
  bool error;
  bool synchronized;
  String readingTime;
  bool access;
  String deviceId;
  String errorMessage;

  TicketReading({
    required this.id,
    required this.orderId,
    required this.ticketId,
    required this.checkpointId,
    required this.error,
    required this.synchronized,
    required this.readingTime,
    required this.access,
    required this.deviceId,
    required this.errorMessage,
  });

  Future<Ticket?> getTicket() async {
    return await TicketDB.db.getTicketByID(this.ticketId);
  }

  factory TicketReading.fromMap(Map<String, dynamic> json) => new TicketReading(
      id: json["id"],
      orderId: json["orderId"],
      ticketId: json["ticketId"],
      checkpointId: json["checkpointId"],
      error: (json["error"] == 1 ? true : false),
      synchronized: (json["synchronized"] == 1 ? true : false),
      readingTime: json["readingTime"],
      access: (json["access"] == 1 ? true : false),
      deviceId: json["deviceId"],
      errorMessage: json["errorMessage"]);

  Map<String, dynamic> toMap() => {
        'id': id,
        'orderId': orderId,
        'ticketId': ticketId,
        'checkpointId': checkpointId,
        'error': error,
        'synchronized': synchronized,
        'readingTime': readingTime,
        'access': access,
        'deviceId': deviceId,
        'errorMessage': errorMessage,
      };

  Map<String, dynamic> toJson() => {
        'id': this.id,
        'orderId': this.orderId,
        'ticketId': this.ticketId,
        'checkpointId': this.checkpointId,
        'error': this.error,
        'synchronized': this.synchronized,
        'readingTime': this.readingTime,
        'access': this.access,
        'deviceId': this.deviceId,
        'errorMessage': this.errorMessage
      };
}
