import 'dart:convert';
import 'package:pda_toppfan/Libraries/API/DB/DBLibrary.dart';
import 'package:pda_toppfan/Models/Accreditation.dart';

AccreditationReading itemFromJson(String str) {
  final jsonData = json.decode(str);
  return AccreditationReading.fromMap(jsonData);
}

String itemToJson(AccreditationReading data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class AccreditationReading {
  int id;
  int accreditationId;
  int checkpointID;
  bool error;
  bool synchronized;
  String readingTime;
  bool access;
  String deviceId;
  String errorMessage;

  AccreditationReading({
    required this.id,
    required this.accreditationId,
    required this.checkpointID,
    required this.error,
    required this.synchronized,
    required this.readingTime,
    required this.access,
    required this.deviceId,
    required this.errorMessage,
  });

  Future<Accreditation?> getAccreditation() async {
    return await AccreditationDB.db.getAccreditationByID(this.accreditationId);
  }

  factory AccreditationReading.fromMap(Map<String, dynamic> json) =>
      new AccreditationReading(
          id: json["id"],
          accreditationId: json["accreditationId"],
          checkpointID: json["checkpointID"],
          error: (json["error"] == 0 ? false : true),
          synchronized: (json["synchronized"] == 0 ? false : true),
          readingTime: json["readingTime"],
          access: (json["access"] == 0 ? false : true),
          deviceId: json["deviceId"],
          errorMessage: json["errorMessage"]);

  Map<String, dynamic> toMap() => {
        'id': id,
        'accreditationId': accreditationId,
        'checkpointID': checkpointID,
        'error': error,
        'synchronized': synchronized,
        'readingTime': readingTime,
        'access': access,
        'deviceId': deviceId,
        'errorMessage': errorMessage,
      };

  Map<String, dynamic> toJson() => {
        'id': this.id,
        'accreditationId': this.accreditationId,
        'checkpointID': this.checkpointID,
        'error': this.error,
        'synchronized': this.synchronized,
        'readingTime': this.readingTime,
        'access': this.access,
        'deviceId': this.deviceId,
        'errorMessage': this.errorMessage,
      };
}
