import 'dart:convert';

Access itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Access.fromMap(jsonData);
}

String itemToJson(Access data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Access {
  int idAccess;
  int userId;
  int venueId;
  String visitorName;
  String start;
  String end;
  String type;

  Access(
      {required this.idAccess,
      required this.userId,
      required this.venueId,
      required this.visitorName,
      required this.start,
      required this.end,
      required this.type});

  factory Access.fromMap(Map<String, dynamic> json) => new Access(
      idAccess: json["idAccess"],
      userId: json["userId"],
      venueId: json["venueId"],
      visitorName: json["visitorName"],
      start: json["start"],
      end: json["end"],
      type: json["type"]);

  Map<String, dynamic> toMap() => {
        'idAccess': idAccess,
        'userId': userId,
        'venueId': venueId,
        'visitorName': visitorName,
        'start': start,
        'end': end,
        'type': type,
      };
}
