import 'dart:convert';

QRReading itemFromJson(String str) {
  final jsonData = json.decode(str);
  return QRReading.fromMap(jsonData);
}

String itemToJson(QRReading data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class QRReading {
  int id;
  String readingType;
  int checkpointId;
  int readingId;
  bool error;
  String readingTime;
  bool access;
  String deviceId;
  String errorMessage;

  QRReading(
      {required this.id,
      required this.readingType,
      required this.checkpointId,
      required this.readingId,
      required this.error,
      required this.readingTime,
      required this.access,
      required this.deviceId,
      required this.errorMessage});

  factory QRReading.fromMap(Map<String, dynamic> json) => new QRReading(
        id: json["id"],
        readingType: json["readingType"],
        checkpointId: json["checkpointId"],
        readingId: json["readingId"],
        error: json["error"],
        readingTime: json["readingTime"],
        access: json["access"],
        deviceId: json["deviceId"],
        errorMessage: json["errorMessage"],
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'readingType': readingType,
        'checkpointId': checkpointId,
        'readingId': readingId,
        'error': error,
        'readingTime': readingTime,
        'access': access,
        'deviceId': deviceId,
        'errorMessage': errorMessage
      };

  Map<String, dynamic> toJson() => {
        'id': this.id,
        'readingType': this.readingType,
        'checkpointId': this.checkpointId,
        'readingId': this.readingId,
        'error': this.error,
        'readingTime': this.readingTime,
        'access': this.access,
        'deviceId': this.deviceId,
        'errorMessage': this.errorMessage
      };
}
