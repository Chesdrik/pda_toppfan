import 'dart:convert';

Checkpoint itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Checkpoint.fromMap(jsonData);
}

String itemToJson(Checkpoint data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Checkpoint {
  int id;
  String name;
  int parent;
  int idVenue;
  String type;

  Checkpoint(
      {required this.id,
      required this.name,
      required this.parent,
      required this.idVenue,
      required this.type});

  factory Checkpoint.fromMap(Map<String, dynamic> json) => new Checkpoint(
        id: json["id"],
        name: json["name"],
        parent: json["parent"],
        idVenue: json["idVenue"],
        type: json["type"],
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'name': name,
        'parent': parent,
        'idVenue': idVenue,
        'type': type
      };

  Map<String, dynamic> toJson() => {
        'id': this.id,
        'name': this.name,
        'parent': this.parent,
        'idVenue': this.idVenue,
        'type': this.type,
      };
}
