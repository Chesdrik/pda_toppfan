import 'dart:convert';

Ticket itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Ticket.fromMap(jsonData);
}

String itemToJson(Ticket data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Ticket {
  int id;
  int ticketId;
  int eventId;
  int ticketTypeId;
  String name;
  int orderId;
  int amount;
  List<int> checkpoints;

  Ticket({
    required this.id,
    required this.ticketId,
    required this.eventId,
    required this.ticketTypeId,
    required this.name,
    required this.orderId,
    required this.amount,
    required this.checkpoints,
  });

  factory Ticket.fromMap(Map<String, dynamic> json) => new Ticket(
        id: json["id"],
        ticketId: json["ticketId"],
        eventId: json["eventId"],
        ticketTypeId: json["ticketTypeId"],
        name: json["name"],
        orderId: json["orderId"],
        amount: json["amount"],
        checkpoints: json["checkpoints"].cast<int>(),
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'ticketId': ticketId,
        'eventId': eventId,
        'ticketTypeId': ticketTypeId,
        'name': name,
        'orderId': orderId,
        'amount': amount,
        'checkpoints': checkpoints,
      };
}
