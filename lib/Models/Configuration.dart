import 'dart:convert';

Configuration itemFromJson(String str) {
  final jsonData = json.decode(str);
  return Configuration.fromMap(jsonData);
}

String itemToJson(Configuration data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Configuration {
  int id;
  int venueId;
  String venueName;
  int checkpointID;
  String checkpointName;
  int checkpointParent;
  String type;
  int idEvent;
  int syncTime;

  Configuration(
      {required this.id,
      required this.venueId,
      required this.venueName,
      required this.checkpointID,
      required this.checkpointName,
      required this.checkpointParent,
      required this.type,
      required this.idEvent,
      required this.syncTime});

  factory Configuration.fromMap(Map<String, dynamic> json) => new Configuration(
      id: json["id"],
      venueId: json["venueId"],
      venueName: json["venueName"],
      checkpointID: json['checkpointID'],
      checkpointName: json['checkpointName'],
      checkpointParent: json['checkpointParent'],
      type: json['type'],
      idEvent: json['idEvent'],
      syncTime: json['syncTime']);

  Map<String, dynamic> toMap() => {
        'id': id,
        'venueId': venueId,
        'venueName': venueName,
        'checkpointID': checkpointID,
        'checkpointName': checkpointName,
        'checkpointParent': checkpointParent,
        'type': type,
        'idEvent': idEvent,
        'syncTime': syncTime
      };

  Map<String, dynamic> toJson() => {
        'id': this.id,
        'venueId': this.venueId,
        'venueName': this.venueName,
        'checkpointID': this.checkpointID,
        'checkpointName': this.checkpointName,
        'checkpointParent': this.checkpointParent,
        'type': this.type,
        'idEvent': this.idEvent,
        'syncTime': this.syncTime
      };
}
